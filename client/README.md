<!--
  ~
  ~ Copyright (C) 2017-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
  ~
  ~ This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
  ~ If a copy of the MPL was not distributed with this file, You can obtain one at
  ~ https://www.mozilla.org/en-US/MPL/2.0/
  ~
  -->
# ASCLEPIOS ABAC Authorization Client Library (PEP)

The *ABAC authorization client library* is part of ASCLEPIOS ABAC Authorization project.
It's purpose is to make easier the task of adding authorization capability to a protected application (your application).
ABAC client library contains a Servlet filter that must be wired to the HTTP request processing flow of the protected application.
This way ABAC client will be able to intercept incoming requests, extract the needed information, and query ABAC server if it's ok to let HTTP request processing continue. 
It also encompasses a client for contacting ABAC Server and querying whether a request can be allowed to continue or not.
In XACML Architecture, ABAC client library provides a Policy Enforcement Point (PEP).

**NOTES:**
1. You must use **Java 8**.
2. You need Gitlab's username/password in order to download source code.


---
## Build ABAC Client library

Completing the following tasks will install ABAC client library in local Maven repository.
This is necessary in order to continue to the next sections.

*Requirements:*
- You will need to provide your Gitlab credentials in order to retrieve the source code.

1. Clone the project from Gitlab:
    ```
    git clone https://gitlab.com/asclepios-project/abac-authorization.git
    ```
2. Build the project:
    ```
    cd abac-authorization
    mvn clean install
   ```
   
Now ABAC library is installed in local Maven repository.

---
## Add ABAC client (PEP) in a standalone Tomcat

1. Copy JAR file at `abac-authorization/client-tomcat/target/abac-authorization-client-for-tomcat-3.0.3-jar-with-dependencies.jar`
   into Tomcat libraries directory (e.g. `${CATALINA_HOME}/libs`).
   
    ```
    cp abac-authorization/client-tomcat/target/abac-authorization-client-for-tomcat-3.0.3-jar-with-dependencies.jar ${CATALINA_HOME}/libs 
    ```

2. Copy the truststore file of ABAC server into Tomcat home directory (`CATALINA_HOME`).
   You can copy it at any location, but you will need to change the following commands accordingly.
   
    ```
    cp abac-authorization/server/src/main/resources/config/pdp-server-truststore.p12 ${CATALINA_HOME}/truststore-client.p12 
    ```

3. Add the following lines in `web.xml` file of Tomcat (at `${CATALINA}/conf/` directory) in order to wire ABAC client's Servlet filter into Tomcat's request processing flow.

    ```
    <filter>
        <filter-name>AsclepiosAbacFilter</filter-name>
        <filter-class>eu.asclepios.authorization.abac.client.AuthorizationServiceServletFilter</filter-class>
    </filter>
    <filter-mapping>
        <filter-name>AsclepiosAbacFilter</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>
    ```

    Modifying `url-pattern` it's possible to protected specific Tomcat contexts and applications.
    It's also possible to add previous lines in an application's `web.xml` file rather than Tomcat's in order to protect that particular application.

4. Set environment variables for configuring ABAC client. The following environment variables can be set
   in command line just before starting up Tomcat.

    In Linux:
    
    ```
    AZ_CALL_DISABLED=false
    AZ_CALL_LOAD_BALANCE_METHOD=ORDER
    AZ_CALL_RETRIES=1
    AZ_SERVER_ENDPOINTS=https://localhost:7071/checkJsonAccessRequest
    AZ_SERVER_ACCESS_KEY=7235687126587231675321756752657236156321765723
    AZ_CLIENT_TRUST_STORE_FILE=truststore-client.p12
    AZ_CLIENT_TRUST_STORE_TYPE=PKCS12
    AZ_CLIENT_TRUST_STORE_PASSWORD=asclepios
    
    export AZ_CLIENT_TRUST_STORE_FILE AZ_CLIENT_TRUST_STORE_TYPE AZ_CLIENT_TRUST_STORE_PASSWORD AZ_SERVER_ENDPOINTS AZ_SERVER_ACCESS_KEY AZ_CALL_DISABLED AZ_CALL_LOAD_BALANCE_METHOD AZ_CALL_RETRIES
    $CATALINE_HOME/bin/startup.sh
    ```

    In Windows:
    
    ```
    set AZ_CALL_DISABLED=false
    set AZ_CALL_LOAD_BALANCE_METHOD=ORDER
    set AZ_CALL_RETRIES=1
    set AZ_SERVER_ENDPOINTS=https://localhost:7071/checkJsonAccessRequest
    set AZ_SERVER_ACCESS_KEY=7235687126587231675321756752657236156321765723
    set AZ_CLIENT_TRUST_STORE_FILE=truststore-client.p12
    set AZ_CLIENT_TRUST_STORE_TYPE=PKCS12
    set AZ_CLIENT_TRUST_STORE_PASSWORD=asclepios

    %CATALINE_HOME%\bin\startup.bat
    ```

After completing these tasks, any application deployed in Tomcat will be protected by ABAC client.

---
## Add ABAC client (PEP) in a Spring Boot Web application

Depending on the nature of the application, it might require changes in
application's codebase or just configuration.

1. Add ABAC client dependency in application's dependency management system.

    In Maven, you will need to add the following in `pom.xml`:
    
    ```
        <dependencies>
            .....
            <dependency>
                <groupId>eu.asclepios.authorization.abac</groupId>
                <artifactId>abac-authorization-client</artifactId>
                <version>3.0.3</version>
            </dependency>
            .....
        </dependencies>
    ```

2. Copy the truststore file of ABAC server into application's resources directory.
   Alternatively, you can copy it at any location, but you will need to change the following commands accordingly.
   **Take care not exposing this file to public.** 
   
    ```
    cp abac-authorization/server/src/main/resources/config/pdp-server-truststore.p12 <YOUR_APP_HOME>/src/main/resources/truststore-client.p12
    ```

3. Modify application's code or configuration, in order to instruct Spring boot to scan 
   package `eu.asclepios.authorization.abac.client` package. This should initialize and 
   autowire ABAC client's Servlet filter in application's web server.
   Alternatively you can implement a Servlet filter that (explicitly) calls ABAC client
   library facilities. The next code block gives a sample Filter class.
   
   ```java
    package my.example.app;
    
    import eu.asclepios.authorization.abac.util.CheckAccessResponse;
    import org.springframework.stereotype.Component;
    
    import javax.servlet.*;
    import javax.servlet.http.HttpServletRequest;
    import javax.servlet.http.HttpServletResponse;
    import java.io.IOException;
    
    @Component
    public class AuthorizationFilter implements Filter {
        private boolean authorizationDisabled;
    
        @Override
        public void init(final FilterConfig filterConfig) {
            // Initialize authorization service client
            authorizationDisabled = eu.asclepios.authorization.abac.client.WebAccessRequestHelper
                    .getSingleton().getAuthorizationServiceClient().getProperties().getPdp().isDisabled();
        }
    
        @Override
        public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
            HttpServletRequest req = (HttpServletRequest) request;
            HttpServletResponse res = (HttpServletResponse) response;
    
            if (checkAccess(req, res)) {
                filterChain.doFilter(request, response);
            }
        }
    
        private boolean checkAccess(HttpServletRequest servletRequest, HttpServletResponse servletResponse) throws IOException {
            if (authorizationDisabled) {
                return true;
            }
    
            CheckAccessResponse response;
            boolean permit;
            try {
                response = eu.asclepios.authorization.abac.client.WebAccessRequestHelper.getSingleton()
                        .getCheckWebAccessResponse(servletRequest, "resource", "action", "subject", null);
                permit = (response == null) || response.isPermit();
            } catch (Exception e) {
                permit = false;
            }
    
            if (! permit) {
                servletResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "YOU CANNOT ACCESS THE SENSITIVE DATA");
                return false;
            } else {
                return true;
            }
        }
    }
   ```

4. Set environment variables for configuring ABAC client, like in the previous section. 
   Environment variables can be set in command line just before starting up application.
   
   <span style="color:red">**-- TODO --**</span>
   
   Alternatively you can add `authorization-client.properties` file in your application's resources.
   ABAC client will look for it if environment variables are not provided.
   

#### *[OPTIONAL]* Sample Spring boot application

You can experiment with a simple application, used for testing ABAC server. Its code is an example of adding client-side authorization with ABAC client library.
This is a bare bones Spring boot Web application using the Servlet filter of ABAC client. Spring boot takes care of wiring it (the filter) into Tomcat so authorization will work immediately.
If your application does not use Spring boot you will probably need to write some code in order to wire the filter into the Web server.

1.  Clone the project from Gitlab:

    ```
    git clone https://gitlab.com/asclepios-project/simple-client-app.git
    ```

2.  Build the project:

    ```
    cd simple-client-app
    mvn clean package
    ```
    
3.  Run the application

    In Linux: `run.bat`
    
    In Windows: `./run.sh`

4. Start ABAC server (see README in ABAC server module)

5. Using a browser try accessing page http://localhost:8080.
   If everything worked ok you should see a message `You can access sensitive data!`

   You can also try the opposite case (i.e. access denied). An easy way to achieve this is by deleting the policy files (*.xml) from ABAC Server's policies folder.
   For example:

    In Linux: `rm abac-authorization/server/src/main/resources/policies/*.xml`
    
    In Windows: `del abac-authorization\server\src\main\resources\policies\*.xml`

   Deleting the policy files will leave ABAC server with no policies (which will cause an error that ABAC client will translate to Deny Access). Next, refresh the 
   page at http://localhost:8080. This time an `HTTP 401 (Unauthorized)` error should come up.

---