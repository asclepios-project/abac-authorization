/*
 * Copyright (C) 2017-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.asclepios.authorization.abac.client;

import eu.asclepios.authorization.abac.util.*;
import eu.asclepios.authorization.abac.util.json.JsonSerializer;
import eu.asclepios.authorization.abac.util.json.JsonUnserializer;
import eu.asclepios.authorization.abac.util.properties.AuthorizationServiceClientProperties;
import eu.asclepios.authorization.abac.util.properties.HttpClientProperties;
import lombok.Getter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public class AuthorizationServiceClient {
	
	private final AuthorizationServiceClientProperties properties;
	@Getter
	private final boolean disabled;
	private final String pdpJsonEndpoint;
	private final String pdpAccessToken;
	private final String[] pdpEndpoints;
	private final RestClient restClient;
	//private final AuthorizationServiceClientProperties.LOAD_BALANCE_METHOD loadBalanceMethod;
	private final String loadBalanceMethod;
	private final int maxPdpConnectRetries;
	private int currentEndpoint = -1;

	private static AuthorizationServiceClientProperties getConfigurationFromEnvironment() {
		// retrieve PDP client configuration from Environment variables
		AuthorizationServiceClientProperties properties = new AuthorizationServiceClientProperties();
		AuthorizationServiceClientProperties.PdpConfig pdpCfg = new AuthorizationServiceClientProperties.PdpConfig();
		properties.setPdp(pdpCfg);

		pdpCfg.setDisabled(Boolean.parseBoolean(getenv("CALL_DISABLED", "false")));
		pdpCfg.setEndpoints(Arrays.stream(getenv("SERVER_ENDPOINTS", "").split(","))
				.map(String::trim).filter(StringUtils::isNotBlank).toArray(String[]::new));
		pdpCfg.setAccessKey(getenv("SERVER_ACCESS_KEY", null));
		pdpCfg.setLoadBalanceMethod(getenv("CALL_LOAD_BALANCE_METHOD", "ORDER").toUpperCase());
		pdpCfg.setRetryCount(Math.max(Integer.parseInt(getenv("CALL_RETRIES", "0")), 0));

		// retrieve HTTP client configuration from Environment variables
		String trustStoreFile = getenv("CLIENT_TRUST_STORE_FILE", null);
		String trustStoreType = getenv("CLIENT_TRUST_STORE_TYPE", "PKCS");
		String trustStorePassword = getenv("CLIENT_TRUST_STORE_PASSWORD", null);
		if (StringUtils.isNotBlank(trustStoreFile)) {
			HttpClientProperties httpClientProperties = new HttpClientProperties();
			pdpCfg.setHttpClient(httpClientProperties);

			httpClientProperties.setKeystoreFile(trustStoreFile);
			httpClientProperties.setKeystoreFileType(trustStoreType);
			httpClientProperties.setKeystorePassword(trustStorePassword);
		}

		return properties;
	}

	private static String getenv(String s, String devaultValue) {
		String val = System.getenv("ASCLEPIOS_AZ_"+s);
		if (val==null) val = System.getenv("AZ_"+s);
		if (val==null) val = devaultValue;
		return val;
	}

	public AuthorizationServiceClient() {
		this(null);
	}

	public AuthorizationServiceClient(AuthorizationServiceClientProperties properties) {
		if (properties==null)
			properties = getConfigurationFromEnvironment();
		log.debug("New AuthorizationServiceClient: properties: {}", properties);

		// retrieve PDP client configuration
		this.properties = properties;
		disabled = properties.getPdp().isDisabled();
		pdpJsonEndpoint = properties.getPdp().getEndpoints().length>0
				? StringUtils.trim(properties.getPdp().getEndpoints()[0])
				: null;
		//PDP_JSON_ENDPOINT = properties.getPdp().getBaseUrl().trim();
		pdpAccessToken = StringUtils.trimToEmpty(properties.getPdp().getAccessKey());
		pdpEndpoints = properties.getPdp().getEndpoints();
		if (StringUtils.isBlank(properties.getPdp().getLoadBalanceMethod())) {
			loadBalanceMethod = "ORDER";
		} else {
			loadBalanceMethod = StringUtils.upperCase(StringUtils.trim(properties.getPdp().getLoadBalanceMethod()));
		}
		maxPdpConnectRetries = properties.getPdp().getRetryCount() > 0
				? properties.getPdp().getRetryCount()
				: 0;

		printConfig();

		// create restClient (backed by a configurable http client)
		try {
			String url = pdpEndpoints.length>0 ? StringUtils.trimToEmpty(pdpEndpoints[0]) : "";
			restClient = RestClientUtil.createRestClient(properties.getPdp().getHttpClient(), url);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	private void printConfig() {
		log.info("Authorization Service client settings:");
		log.info(" * Client Enabled:    {}", ! disabled);
		log.info(" * PDP endpoints:     {}", Arrays.toString(pdpEndpoints));
		log.info(" * PDP Access Token:  {}", ! pdpAccessToken.isEmpty() ? "present" : "missing");
		log.info(" * Load-Balancing:    {}", loadBalanceMethod);
		log.info(" * Retry count:       {}", maxPdpConnectRetries);

		HttpClientProperties httpProps = properties.getPdp().getHttpClient();
		if (httpProps!=null) {
			log.info("HTTPS client settings:");
			log.info(" * Trust store file:     {}", httpProps.getKeystoreFile());
			log.info(" * Trust store type:     {}", httpProps.getKeystoreFileType());
			log.info(" * Trust store password: {}", StringUtils.isBlank(httpProps.getKeystorePassword()) ? "not set" : "present");
		} else {
			log.info("HTTPS client settings: Not provided");
		}

		if (disabled) { log.warn("Authorization Service client is DISABLED"); }
	}
	
	public AuthorizationServiceClientProperties getProperties() {
		return properties;
	}

	protected synchronized String _getNextEndpoint(boolean forceNext) {
		if ("ROUND_ROBIN".equals(loadBalanceMethod)) {
			if (pdpEndpoints!=null && pdpEndpoints.length>0) {
				currentEndpoint = (++currentEndpoint) % pdpEndpoints.length;
			}
		} else
		if ("RANDOM".equals(loadBalanceMethod)) {
			if (pdpEndpoints!=null && pdpEndpoints.length>0) {
				currentEndpoint = (int)(Math.random() * pdpEndpoints.length);
			}
		} else
		// ORDER, HASH and default
		{
			if (pdpEndpoints!=null && pdpEndpoints.length>0 && (forceNext || currentEndpoint<0)) {
				currentEndpoint = (++currentEndpoint) % pdpEndpoints.length;
			}
		}
		return getPdpEndpoint();
	}
	
	public String getPdpEndpoint() {
		return currentEndpoint<0 ? pdpJsonEndpoint : pdpEndpoints[currentEndpoint];
	}
	
	public boolean requestAccess(String resource, String action, String subject) throws AccessDeniedException {
		return requestAccess(resource, action, subject, null, true);
	}
	
	public boolean requestAccess(String resource, String action, String subject, Map<String,Object> extra) throws AccessDeniedException {
		return requestAccess(resource, action, subject, extra, true);
	}

	public boolean requestAccess(String resource, String action, String subject, Map<String,Object> extra, boolean exceptionIfNotPermit) throws AccessDeniedException {
		if (disabled) { log.debug("Authorization Service client is disabled"); return true; }

		// Collect access request attributes into a Map
		HashMap<String, Object> attrs = putAccessRequestAttributesIntoMap(resource, action, subject, extra);

		// request access from PDP
		boolean permit = requestAccess( attrs );
		checkPermit(resource, action, subject, exceptionIfNotPermit, permit);

		return permit;
	}

	public boolean requestAccess(Map<String,Object> attrs) {
		if (disabled) { log.debug("Authorization Service client is disabled"); return true; }
		CheckAccessResponse response = getRequestAccessResponse(attrs);
		return response.isPermit();
	}

	static void checkPermit(String resource, String action, String subject, boolean exceptionIfNotPermit, boolean permit) throws AccessDeniedException {
		if (!permit && exceptionIfNotPermit)
			throw new AccessDeniedException( String.format("'%s' cannot '%s' resource '%s'", subject, action, resource) );
	}

	private HashMap<String, Object> putAccessRequestAttributesIntoMap(String resource, String action, String subject, Map<String, Object> extra) {
		HashMap<String, Object> attrs = new HashMap<>();
		if (extra != null) attrs.putAll(extra);

		attrs.put("actionId", action);
		attrs.put("resource-id", resource);
		attrs.put("user.identifier", (subject != null && !subject.isEmpty()) ? subject : pdpAccessToken);
		return attrs;
	}

	public CheckAccessResponse getRequestAccessResponse(String resource, String action, String subject, Map<String,Object> extra) {
		if (disabled) { log.debug("Authorization Service client is disabled"); return null; }

		// Collect access request attributes into a Map
		HashMap<String, Object> attrs = putAccessRequestAttributesIntoMap(resource, action, subject, extra);

		// request access from PDP
		return getRequestAccessResponse(attrs);
	}

	@SneakyThrows
	public CheckAccessResponse getRequestAccessResponse(Map<String,Object> attrs) {
		if (disabled) { log.debug("Authorization Service client is disabled"); return null; }
		
		// Create request to PDP
		CheckAccessRequest request = new CheckAccessRequest();
		request.setId( java.util.UUID.randomUUID().toString() );
		request.setContent( null );
		request.setTimestamp( System.currentTimeMillis() );
		request.setXacmlRequest( attrs );
		
		// Serialize access request to a json message
		log.trace("Request object to serialize into JSON message: {}", request);
		String jsonRequest = new JsonSerializer().serializeObject(request);
		log.trace("JSON message to submit: {}", jsonRequest);
		
		// Contact PDP
		CheckAccessResponse response = null;
		String jsonResponse = null;
		boolean forceNext = false;
		boolean repeat;
		int retryCount = 0;
		do {
			repeat = false;
			try {
				String endpoint = _getNextEndpoint(forceNext);
				log.debug("Access Request to PDP: {} - {}", endpoint, jsonRequest);
				//response = restClient.post( endpoint, request, CheckAccessResponse.class );
				jsonResponse = restClient.post( endpoint, jsonRequest, pdpAccessToken );
				
				// Unserialize access response from json message
				log.debug("JSON message received from PDP: {}", jsonResponse);
				if (StringUtils.isNotBlank(jsonResponse)) {
					try {
						response = (CheckAccessResponse) new JsonUnserializer().unserializeObject(jsonResponse);
					} catch (Exception e) {
						log.error("Could not parse JSON response: ", e);
						response = new CheckAccessResponse();
						response.setPdpResult(new PdpResult("DENY", null, null, e.getMessage(), null));
					}
				} else {
					response = new CheckAccessResponse();
					response.setPdpResult(new PdpResult("DENY", null, null, "Empty response returned", null));
				}
				//log.trace("JSON message unserialized to a response object: {}", response);
				log.debug("Access Response from PDP: {}", response);
				
			} catch (Throwable th) {
				log.warn("Access Request to PDP failed: ", th);
				if ("ORDER".equals(loadBalanceMethod)) {
					retryCount++;
					if (retryCount<maxPdpConnectRetries) {
						log.warn("Retrying #{}...", retryCount);
						repeat = true;
						forceNext = true;
					} else {
						log.error("Retry count exceeded");
						throw th;
					}
				} else {
					log.error("No load balance method. Failing immediately");
					throw th;
				}
			}
		} while (repeat);

		//String decision = response.getContent();
		//boolean permit = ( decision!=null && decision.equalsIgnoreCase("PERMIT") );

		return response;
	}
}
