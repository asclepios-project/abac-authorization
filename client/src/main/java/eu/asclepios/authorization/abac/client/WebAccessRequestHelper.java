/*
 * Copyright (C) 2017-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.asclepios.authorization.abac.client;

import eu.asclepios.authorization.abac.context.ContextClientUtil;
import eu.asclepios.authorization.abac.context.ContextRepositoryClient;
import eu.asclepios.authorization.abac.context.extractor.HttpRequestContextExtractor;
import eu.asclepios.authorization.abac.util.CheckAccessResponse;
import eu.asclepios.authorization.abac.util.properties.AuthorizationServiceClientProperties;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.keycloak.KeycloakPrincipal;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.representations.AccessToken;
import org.keycloak.representations.AddressClaimSet;
import org.keycloak.representations.IDToken;
import org.springframework.security.core.Authentication;
import org.springframework.util.StreamUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.charset.Charset;
import java.util.*;

@Slf4j
public class WebAccessRequestHelper {

	private static final String JWT_TOKEN_PREFIX = "Bearer ";
	private static final String JWT_HEADER_STRING = "Authorization";
	private static String jwtSecret;

	private static WebAccessRequestHelper singleton;
	
	public synchronized static WebAccessRequestHelper getSingleton(AuthorizationServiceClientProperties properties) {
		if (singleton==null) singleton = new WebAccessRequestHelper( properties );
		return singleton;
	}
	
	public synchronized static WebAccessRequestHelper getSingleton(AuthorizationServiceClientProperties properties, boolean useConfiguredAccessKey, boolean useRequestAccessKey, boolean configuredAccessKeyOverrides) {
		if (singleton==null) singleton = new WebAccessRequestHelper( properties, useConfiguredAccessKey, useRequestAccessKey, configuredAccessKeyOverrides );
		return singleton;
	}
	
	public synchronized static WebAccessRequestHelper getSingleton() {
		if (singleton==null)
			singleton = new WebAccessRequestHelper();
		return singleton;
	}
	
	// --------------------------------------------------------------------------------------------
	// Fields, constants and constructors
	
	public final static String REQUEST_ID_ATTRIBUTE_NAME = "eu.asclepios.authorization.abac.context.WebAccessRequestHelper.REQUEST-ID";
	
	private AuthorizationServiceClient pdpClient;
	private ContextRepositoryClient contextReposClient;
	private HttpRequestContextExtractor extractor;
	private boolean useConfiguredAccessKey = true;
	private boolean useRequestAccessKey = true;
	private boolean configuredAccessKeyOverrides = false;
	private String accessKey = null;
	
	private WebAccessRequestHelper() { this(null); }
	
	private WebAccessRequestHelper(AuthorizationServiceClientProperties properties) {
		// initialize PDP client
		this.pdpClient = properties!=null
				? new AuthorizationServiceClient(properties)
				: new AuthorizationServiceClient();
		properties = pdpClient.getProperties();
		if (properties==null) return;

		// initialize client-side request context extraction, if configured
		this.extractor = (HttpRequestContextExtractor) ContextClientUtil.createExtractor( properties.getContext() );
		this.contextReposClient = (extractor!=null) ? extractor.getContextRepositoryClient() : null;

		// initialize JWT secret
		setJwtSecret(properties.getJwtSecret());
	}

	private WebAccessRequestHelper(AuthorizationServiceClientProperties properties, boolean useConfiguredAccessKey, boolean useRequestAccessKey, boolean configuredAccessKeyOverrides) {
		this(properties);
		this.useConfiguredAccessKey = useConfiguredAccessKey;
		this.useRequestAccessKey = useRequestAccessKey;
		this.configuredAccessKeyOverrides = configuredAccessKeyOverrides;
		this.accessKey = properties.getPdp().getAccessKey();
	}
	
	// --------------------------------------------------------------------------------------------
	// Getter methods
	
	public AuthorizationServiceClient getAuthorizationServiceClient() {
		return this.pdpClient;
	}
	
	public HttpRequestContextExtractor getHttpRequestContextExtractor() {
		return this.extractor;
	}
	
	public ContextRepositoryClient getContextRepositoryClient() {
		return this.contextReposClient;
	}
	
	// --------------------------------------------------------------------------------------------
	// Web Access Request methods
	
	public boolean requestWebAccess(HttpServletRequest request, HttpServletResponse response) throws java.io.IOException {
		if (pdpClient.isDisabled()) {
			log.debug("Authorization Service is disabled. Request is permitted");
			return true;
		}

		log.debug("Request:    {} {}", request.getMethod(), request.getRequestURL());
		log.debug("Access Key: {}", request.getHeader("x-access-key"));
		
		boolean permit = false;
		String mesg = null;
		try {
			permit = checkWebAccessFromRequest(request);
			if (!permit) mesg = "Not authorized access to resource: "+request.getRequestURI();
		} catch (Exception ex) {
			mesg = "An error occurred while evaluating access request";
		}
		
		// Handle response in case of authorization failure (deny/error etc)
		if (!permit) {
			log.warn("Access Denied to web request: {} - {}", request.getRequestURI(), mesg);
			response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			new java.io.PrintStream(response.getOutputStream()).println(mesg);
		} else {
			log.info("Web Request authorized: {}", request.getRequestURI());
		}
		return permit;
	}
	
	// --------------------------------------------------------------------------------------------

	public boolean checkWebAccessFromRequest(HttpServletRequest request) throws AccessDeniedException {
		return checkWebAccessFromRequest(request, null, null, null, null, true);
	}

	public boolean checkWebAccessFromRequest(HttpServletRequest request, String resource, String action, String subject, Map<String,Object> extraInfo, boolean exceptionIfNotPermit) throws AccessDeniedException {
		if (pdpClient.isDisabled()) {
			log.debug("Authorization Service is disabled. Request is permitted");
			return true;
		}

		CheckAccessResponse response = getCheckWebAccessResponse(request, resource, action, subject, extraInfo);
		boolean permit = response!=null && response.isPermit();
		if (permit) return true;
		AuthorizationServiceClient.checkPermit(resource, action, subject, exceptionIfNotPermit, permit);
		return false;
	}

	public CheckAccessResponse getCheckWebAccessResponse(HttpServletRequest request, String resource, String action, String subject, Map<String,Object> extraInfo) {
		if (pdpClient.isDisabled()) {
			log.debug("Authorization Service is disabled. Request is permitted");
			return new CheckAccessResponse("PERMIT");
		}

		// PEP-level context extraction to occur here
		long requestId = -1;
		if (extractor!=null) {
			log.debug("Sending request context to context server");
			requestId = extractor.extractContext(request);
			if (requestId>=0) request.setAttribute(REQUEST_ID_ATTRIBUTE_NAME, requestId);
			log.debug("Request context stored");
			log.debug("Context server assigned Request-Id: {}", requestId);
		}
		
		// Collect mandatory info needed by PDP client
		if (StringUtils.isBlank(subject)) {
			if (useRequestAccessKey) {
				subject = request.getHeader("x-access-key");
				if (StringUtils.isBlank(subject))
					subject = request.getParameter("x-access-key");	//XXX: POSSIBLY-A-BUG: In case of HTTP POST getParameter() might interfere with reading request body with getInputStream() or getReader()
				if (StringUtils.isBlank(subject))
					subject = getAccessKeyFromQueryString(request.getQueryString());
			}
			if (useConfiguredAccessKey && (StringUtils.isBlank(subject) || configuredAccessKeyOverrides)) {
				subject = accessKey;
			}
			if (StringUtils.isBlank(subject)) subject = "";
		}
		
		if (StringUtils.isBlank(action)) action = request.getMethod();
		if (StringUtils.isBlank(resource)) resource = request.getRequestURI();
		log.debug("PDP client will be invoked with: subject={}, action={}, resource={}", subject, action, resource);
		
		// Collect other HTTP request info as extra request attributes
		//HashMap<String,String> extra = new HashMap<String,String>();
		HashMap<String,Object> extra =
				extraInfo!=null
						? new HashMap<>(extraInfo)
						: new HashMap<>();
		collectRequestAttributes(request, extra);
		
		// Collect JWT token info (if provided) as extra request attributes
		collectJwtAttributes(request, extra);

		// Collect Keycloak info (if provided) as extra request attributes
		collectKeycloakAttributes(request, extra);

		// Collect info from JSON request body
		collectJsonRequestBodyAttributes(request, extra);

		// Use PDP client to evaluate access request against policies
		CheckAccessResponse response;
		//boolean permit = false;
		try {
			if (requestId>=0) {
				extra.put("request-id", Long.toString(requestId));
			}

			log.debug("Extra attributes added: {}", extra);
			log.debug("Calling PDP: {}", pdpClient.getPdpEndpoint());
			//permit = pdpClient.requestAccess(resource, action, subject, extra, exceptionIfNotPermit);
			response = pdpClient.getRequestAccessResponse(resource, action, subject, extra);
			//log.debug("PDP decision: {}", permit);
			log.debug("PDP response: {}", response);
		}
		//catch (AccessDeniedException ignored) { }
		catch (Exception ex) { log.warn("An error occurred while evaluating web access request: ", ex); throw ex; }
		
		//return permit;
		return response;
	}
	
	void clearRequestContext(HttpServletRequest request) {
		Object o = request.getAttribute(REQUEST_ID_ATTRIBUTE_NAME);
		if (o!=null) {
			long requestId = (Long) o;
			if (requestId>=0) {
				log.info("Clearing context for request: {}", requestId);
				this.contextReposClient.deleteContextForRequest(requestId);
			}
		}
	}
	
	// --------------------------------------------------------------------------------------------
	// Non-public methods
	
	protected String getAccessKeyFromQueryString(String qs) {
		log.debug("Query String: {}", qs);
		if (qs!=null) {
			int p;
			if (qs.startsWith("x-access-key=")) p = 0;
			else p = qs.indexOf("&x-access-key=");
			if (p>-1) {
				p = qs.indexOf("=",p);
				p++;
				if (p<qs.length()) {
					int p2 = qs.indexOf("&", p);
					if (p2>p) return qs.substring(p,p2).trim();
					else return qs.substring(p).trim();
				}
			}
		}
		return null;
	}
	
	protected void collectRequestAttributes(HttpServletRequest request, Map<String,Object> infoMap) {
		// Add HTTP request headers
		Enumeration en = request.getHeaderNames();
		while (en.hasMoreElements()) {
			String header = en.nextElement().toString();
			String str = header.toLowerCase().replaceAll("[^A-Za-z0-9_]", "-");
			infoMap.put("http-header-"+str, request.getHeader(header));
		}
		// Add HTTP request cookies
		if (request.getCookies()!=null) {
			for (Cookie cookie : request.getCookies()) {
				String str = cookie.getName().toLowerCase().replaceAll("[^A-Za-z0-9_]", "-");
				infoMap.put("http-cookie-" + str, cookie.getValue());
			}
		}
		// Add HTTP request attributes
		/*for (Object attr : Collections.list(request.getAttributeNames())) {
			String str = attr.toString().toLowerCase().replaceAll("[^A-Za-z0-9_]", "-");
			Object val = request.getAttribute(attr.toString());
			if (val!=null)
				infoMap.put("http-attr-"+str, val.toString());
		}*/
		// Add HTTP request parameters
		for (Object param : Collections.list(request.getParameterNames())) {
			String str = param.toString().toLowerCase().replaceAll("[^A-Za-z0-9_]", "-");
			infoMap.put("http-param-"+str, request.getParameter(param.toString()));
		}
		//infoMap.put("http-req-parameter-map", httpRequest.getParameterMap());

		// Add HTTP request info
		infoMap.put("http-req-auth-type", request.getAuthType());
		infoMap.put("http-req-context-path", request.getContextPath());
		infoMap.put("http-req-method", request.getMethod());
		infoMap.put("http-req-servlet-path", request.getServletPath());
		infoMap.put("http-req-query-string", request.getQueryString());
		infoMap.put("http-req-remote-user", request.getRemoteUser());
		infoMap.put("http-req-requested-session-id", request.getRequestedSessionId());
		infoMap.put("http-req-uri", request.getRequestURI());
		infoMap.put("http-req-url", request.getRequestURI());
		//infoMap.put("http-req-base-url", grailsWebRequest.getBaseUrl());
		infoMap.put("http-session-valid", request.isRequestedSessionIdValid());
		if (request.getSession()!=null) {
			infoMap.put("http-session-id", request.getSession().getId());
			infoMap.put("http-session-creation-time", request.getSession().getCreationTime());
			infoMap.put("http-session-last-accessed-time", request.getSession().getLastAccessedTime());
			infoMap.put("http-session-max-inactive-interval", request.getSession().getMaxInactiveInterval());
		}
		infoMap.put("http-req-is-secure", request.isSecure());
		infoMap.put("http-req-content-type", request.getContentType());
		infoMap.put("http-req-content-length", request.getContentLength());
		infoMap.put("http-req-character-encoding", request.getCharacterEncoding());
		infoMap.put("http-req-local-address", request.getLocalAddr());
		infoMap.put("http-req-local-port", request.getLocalPort());
		infoMap.put("http-req-local-name", request.getLocalName());
		infoMap.put("http-req-protocol", request.getProtocol());
		infoMap.put("http-req-remote-address", request.getRemoteAddr());
		infoMap.put("http-req-remote-host", request.getRemoteHost());
		infoMap.put("http-req-remote-port", request.getRemotePort());
		infoMap.put("http-req-scheme", request.getScheme());
		infoMap.put("http-req-server-name", request.getServerName());
		infoMap.put("http-req-server-port", request.getServerPort());

		log.debug("HTTP request info: {}", infoMap);
	}

	/*protected void collectRequestAttributes(HttpServletRequest request, Map<String,Object> extra) {
		// print request attributes
		log.debug("auth-type:      {}", request.getAuthType());
		log.debug("method:         {}", request.getMethod());
		log.debug("protocol:       {}", request.getProtocol());
		log.debug("scheme:         {}", request.getScheme());
		log.debug("secure:         {}", request.isSecure());
		//log.debug("cookies:        {}", request.getCookies());
		log.debug("parameters:     {}", prepareParameterMap( request.getParameterMap() ));
		log.debug("encoding:       {}", request.getCharacterEncoding());
		log.debug("content-type:   {}", request.getContentType());
		log.debug("remote-address: {}", request.getRemoteAddr());
		log.debug("remote-host:    {}", request.getRemoteHost());
		log.debug("remote-port:    {}", request.getRemotePort());
		log.debug("session-valid:  {}", request.isRequestedSessionIdValid());
		
		// Add collected info to 'extra' map
		extra.put("auth-type", request.getAuthType());
		extra.put("method", request.getMethod());
		extra.put("protocol", request.getProtocol());
		extra.put("scheme", request.getScheme());
		extra.put("secure", request.isSecure());
		//extra.put("cookies", request.getCookies());		//XXX:BUG: This line seems to cause self-recursion when serializing cookies field
		extra.put("parameters", new HashMap( request.getParameterMap() ));
		extra.put("encoding", request.getCharacterEncoding());
		extra.put("content-type", request.getContentType());
		extra.put("remote-address", request.getRemoteAddr());
		extra.put("remote-host", request.getRemoteHost());
		extra.put("remote-port", request.getRemotePort());
		extra.put("session-valid", request.isRequestedSessionIdValid());
	}*/

	public void setJwtSecret(String secret) {
		if (StringUtils.isEmpty(secret)) secret = null;
		jwtSecret = secret;
	}

	protected void collectJwtAttributes(HttpServletRequest request, Map<String,Object> extra) {
		// JWT secret has not been set
		if (StringUtils.isEmpty(jwtSecret)) {
			log.debug("collectJwtAttributes(): JWT secret has not been set");
			return;
		}

		// get JWT token from Header
		String token = request.getHeader(JWT_HEADER_STRING);
		if (token == null || !token.startsWith(JWT_TOKEN_PREFIX)) {
			log.debug("collectJwtAttributes(): No Authorization header found or it is not JWT: {}", token);
			return;
		}

		// get claims from JWT token
		Claims claims = Jwts.parser()
				.setSigningKey(jwtSecret.getBytes())
				.parseClaimsJws(token.replace(JWT_TOKEN_PREFIX, ""))
				.getBody();
		log.debug("jwt-audience:   {}", claims.getAudience());
		log.debug("jwt-expiration: {}", claims.getExpiration());
		log.debug("jwt-id:         {}", claims.getId());
		log.debug("jwt-issuedAt:   {}", claims.getIssuedAt());
		log.debug("jwt-issuer:     {}", claims.getIssuer());
		log.debug("jwt-notBefore:  {}", claims.getNotBefore());
		log.debug("jwt-subject:    {}", claims.getSubject());

		// add JWT claims to 'extra' map
		extra.put("jwt-audience", claims.getAudience());
		extra.put("jwt-expiration", claims.getExpiration());
		extra.put("jwt-id", claims.getId());
		extra.put("jwt-issuedAt", claims.getIssuedAt());
		extra.put("jwt-issuer", claims.getIssuer());
		extra.put("jwt-notBefore", claims.getNotBefore());
		extra.put("jwt-subject", claims.getSubject());
	}

	/*protected Map<String,String> prepareParameterMap(Map parameters) {
		Map<String,String> map = new HashMap<String,String>();
		for (Object name : parameters.keySet()) {
			Object value = parameters.get(name);
			String nameStr  = name!=null ? name.toString() : null;
			String valueStr = null;
			if (value!=null) {
				valueStr = value.getClass().isArray()
						? Arrays.toString( (Object[])value )
						: value.toString();
			}
			map.put(nameStr, valueStr);
		}
		return map;
	}*/

	protected void collectKeycloakAttributes(HttpServletRequest request, Map<String,Object> extra) {
		Object userPrincipal = request.getUserPrincipal();
		log.trace("collectKeycloakAttributes: user-principal: class={}, instance={}", userPrincipal!=null?userPrincipal.getClass():null, userPrincipal);
		if (userPrincipal instanceof Authentication) {
			log.trace("collectKeycloakAttributes: user-principal is a KeycloakAuthenticationToken. Extracting principal");
			userPrincipal = ((Authentication)userPrincipal).getPrincipal();
			log.trace("collectKeycloakAttributes: user-principal-2: class={}, instance={}", userPrincipal!=null?userPrincipal.getClass():null, userPrincipal);
		}
		if (userPrincipal instanceof KeycloakPrincipal) {
			KeycloakPrincipal<KeycloakSecurityContext> keycloakPrincipal = (KeycloakPrincipal<KeycloakSecurityContext>) userPrincipal;
			log.trace("collectKeycloakAttributes: kc-principal: class={}, instance={}", keycloakPrincipal.getClass(), keycloakPrincipal);
			KeycloakSecurityContext keycloakSecurityContext = keycloakPrincipal.getKeycloakSecurityContext();
			log.trace("collectKeycloakAttributes: kc-security-context: class={}, instance={}", keycloakSecurityContext!=null?keycloakSecurityContext.getClass():null, keycloakSecurityContext);
			if (keycloakSecurityContext!=null) {
				IDToken idToken = keycloakSecurityContext.getIdToken();
				log.trace("collectKeycloakAttributes: kc-id-token: class={}, instance={}", idToken!=null?idToken.getClass():null, idToken);
				AccessToken accessToken = keycloakSecurityContext.getToken();
				log.trace("collectKeycloakAttributes: kc-access-token: class={}, instance={}", accessToken!=null?accessToken.getClass():null, accessToken);
				if (idToken==null && accessToken!=null) {
					idToken = accessToken;
					log.debug("collectKeycloakAttributes: kc-id-token is NULL. Will use access-token instead: access-token: {}", accessToken.toString());
				}

				if (idToken!=null) {
					log.trace("collectKeycloakAttributes: Extracting info from Keycloak ID Token: {}", idToken);

					// IDToken standard claims
					collectKeycloakClaim("kc-access-token-hash", idToken.getAccessTokenHash(), extra);
					collectKeycloakClaim("kc-acr", idToken.getAcr(), extra);
					collectKeycloakClaim("kc-name", idToken.getName(), extra);

					AddressClaimSet addressClaimSet = idToken.getAddress();
					if (addressClaimSet!=null) {
						collectKeycloakClaim("kc-address-country", addressClaimSet.getCountry(), extra);
						collectKeycloakClaim("kc-address-formatted-address", addressClaimSet.getFormattedAddress(), extra);
						collectKeycloakClaim("kc-address-locality", addressClaimSet.getLocality(), extra);
						collectKeycloakClaim("kc-address-postal-code", addressClaimSet.getPostalCode(), extra);
						collectKeycloakClaim("kc-address-region", addressClaimSet.getRegion(), extra);
						collectKeycloakClaim("kc-address-street-address", addressClaimSet.getStreetAddress(), extra);
					}

					collectKeycloakClaim("kc-auth-time", idToken.getAuth_time(), extra);
					collectKeycloakClaim("kc-birthdate", idToken.getBirthdate(), extra);
					if (idToken.getCategory()!=null) {
						collectKeycloakClaim("kc-category-name", idToken.getCategory().name(), extra);
						collectKeycloakClaim("kc-category-ordinal", idToken.getCategory().ordinal(), extra);
					}
					collectKeycloakClaim("kc-claims-locales", idToken.getClaimsLocales(), extra);
					collectKeycloakClaim("kc-code-hash", idToken.getCodeHash(), extra);
					collectKeycloakClaim("kc-email", idToken.getEmail(), extra);
					collectKeycloakClaim("kc-email-verified", idToken.getEmailVerified(), extra);
					collectKeycloakClaim("kc-family-name", idToken.getFamilyName(), extra);
					collectKeycloakClaim("kc-gender", idToken.getGender(), extra);
					collectKeycloakClaim("kc-given-name", idToken.getGivenName(), extra);
					collectKeycloakClaim("kc-locale", idToken.getLocale(), extra);
					collectKeycloakClaim("kc-middle-name", idToken.getMiddleName(), extra);
					collectKeycloakClaim("kc-nickname", idToken.getNickName(), extra);
					collectKeycloakClaim("kc-nonce", idToken.getNonce(), extra);
					collectKeycloakClaim("kc-phone-number", idToken.getPhoneNumber(), extra);
					collectKeycloakClaim("kc-phone-number-verified", idToken.getPhoneNumberVerified(), extra);
					collectKeycloakClaim("kc-picture", idToken.getPicture(), extra);
					collectKeycloakClaim("kc-preferred-username", idToken.getPreferredUsername(), extra);
					collectKeycloakClaim("kc-profile", idToken.getProfile(), extra);
					collectKeycloakClaim("kc-session-state", idToken.getSessionState(), extra);
					collectKeycloakClaim("kc-state-hash", idToken.getStateHash(), extra);
					collectKeycloakClaim("kc-updated-at", idToken.getUpdatedAt(), extra);
					collectKeycloakClaim("kc-website", idToken.getWebsite(), extra);
					collectKeycloakClaim("kc-zoneinfo", idToken.getZoneinfo(), extra);

					/*List<String> kcAudience = new ArrayList<>();
					Collections.addAll(kcAudience, idToken.getAudience());
					collectKeycloakClaim("kc-audience", kcAudience, extra);*/

					collectKeycloakClaim("kc-exp", idToken.getExp(), extra);
					collectKeycloakClaim("kc-iat", idToken.getIat(), extra);
					collectKeycloakClaim("kc-id", idToken.getId(), extra);
					collectKeycloakClaim("kc-issued-for", idToken.getIssuedFor(), extra);
					collectKeycloakClaim("kc-issuer", idToken.getIssuer(), extra);
					collectKeycloakClaim("kc-nbf", idToken.getNbf(), extra);
					collectKeycloakClaim("kc-subject", idToken.getSubject(), extra);
					collectKeycloakClaim("kc-type", idToken.getType(), extra);
					collectKeycloakClaim("kc-is-active", idToken.isActive(), extra);
					collectKeycloakClaim("kc-is-expired", idToken.isExpired(), extra);

					// IDToken custom claims
					Map<String, Object> customClaims = idToken.getOtherClaims();
					log.debug("collectKeycloakAttributes: custom-claims: {}", customClaims);
					Map<String,Object> customClaimsMap = new HashMap<>();
					customClaims.forEach((key, value) -> {
						log.trace("collectKeycloakAttributes: custom-claim entry: key={}, value={}", key, value);
						String name = "kc-cc--"+key.replaceAll("[^A-Za-z0-9_-]", "_");
						log.trace("collectKeycloakAttributes: custom-claim attribute name: key={}, name={}", key, name);
						String valueStr = value==null ? "" : value.toString();
						collectKeycloakClaim(name, valueStr, customClaimsMap);
					});
					log.debug("collectKeycloakAttributes: custom-claims-map: {}", customClaimsMap);
					extra.putAll(customClaimsMap);

					// IDToken serialization
					try {
						ByteArrayOutputStream baos = new ByteArrayOutputStream();
						ObjectOutputStream oos = new ObjectOutputStream(baos);
						oos.writeObject(idToken);
						byte[] idTokenBytes = baos.toByteArray();
						String idTokenBase64 = Base64.getEncoder().encodeToString(idTokenBytes);
						log.debug("kc-id-token:    {}", idTokenBase64);
						extra.put("kc-id-token", idTokenBase64);
					} catch (IOException e) {
						log.error("Serialization of Keycloak IDToken failed: ", e);
					}

					log.trace("collectKeycloakAttributes: Extracted info from Keycloak ID Token: {}\n{}", idToken, extra);
				}
			}
		}
	}

	protected void collectKeycloakClaim(String name, String value, Map<String,Object> extra) {
		if (StringUtils.isBlank(value)) return;
		_collectKeycloakClaim(name, value, extra);
	}
	protected void collectKeycloakClaim(String name, List value, Map<String,Object> extra) {
		if (value==null || value.size()==0) return;
		_collectKeycloakClaim(name, value, extra);
	}
	protected void collectKeycloakClaim(String name, Integer value, Map<String,Object> extra) {
		if (value==null) return;
		collectKeycloakClaim(name, value.toString(), extra);
	}
	protected void collectKeycloakClaim(String name, Long value, Map<String,Object> extra) {
		if (value==null) return;
		collectKeycloakClaim(name, value.toString(), extra);
	}
	protected void collectKeycloakClaim(String name, Boolean value, Map<String,Object> extra) {
		if (value==null) return;
		collectKeycloakClaim(name, value.toString(), extra);
	}
	protected void _collectKeycloakClaim(String name, Object value, Map<String,Object> extra) {
		if (value==null) return;
		log.debug("_collectKeycloakClaim: claim-name={}, claim-value{}", name, value);
		extra.put(name, value);
	}

	protected void collectJsonRequestBodyAttributes(HttpServletRequest request, Map<String,Object> infoMap) {
		boolean contentCollectionEnabled = pdpClient.getProperties().isContentCollectionEnabled();
		List<String> contentTypes = pdpClient.getProperties().getContentTypesCollected();
		int contentLengthLimit = pdpClient.getProperties().getContentLengthLimit();
		log.trace("collectJsonRequestBodyAttributes: Properties: enabled={}, content-types={}, content-length-limit={}",
				contentCollectionEnabled, contentTypes, contentLengthLimit);

		if (!contentCollectionEnabled || contentTypes==null || contentTypes.size()==0 || contentLengthLimit<=0) {
			log.trace("collectJsonRequestBodyAttributes: Not collecting request body due to configuration: enabled={}, content-types={}, content-length-limit={}",
					contentCollectionEnabled, contentTypes, contentLengthLimit);
			return;
		}

		String type = request.getContentType();
		int len = request.getContentLength();
		log.trace("collectJsonRequestBodyAttributes: Content-type={}, Content-length={}", type, len);
		if (!contentTypes.contains(type)) {
			log.trace("collectJsonRequestBodyAttributes: Request content is *not* JSON: {}", type);
			return;
		}
		if (len>contentLengthLimit) {
			log.trace("collectJsonRequestBodyAttributes: Request content-length is beyond limit: {} > {}",
					len, contentLengthLimit);
			return;
		}

		try {
			log.trace("collectJsonRequestBodyAttributes: Reading request body from stream...");
			log.trace("collectJsonRequestBodyAttributes: Data encoding: {}", request.getCharacterEncoding());
			String content = StreamUtils.copyToString(
					request.getInputStream(), Charset.forName(request.getCharacterEncoding()));
			log.trace("collectJsonRequestBodyAttributes: Request body: {}", content);

			// Collect request content as attribute
			infoMap.put("http-req-content", content);
		} catch (IOException e) {
			log.error("collectJsonRequestBodyAttributes: ERROR while reading request body: ", e);
		}
	}
}