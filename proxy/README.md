<!--
  ~
  ~ Copyright (C) 2017-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
  ~
  ~ This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
  ~ If a copy of the MPL was not distributed with this file, You can obtain one at
  ~ https://www.mozilla.org/en-US/MPL/2.0/
  ~
  -->
# ASCLEPIOS ABAC Authorization Proxy

ABAC Authorization Proxy is a reverse proxy that queries [ABAC PDP server][1] before 
forwarding an incoming request to the target service. It is based on Netflix Zuul.  

[1]: server "ABAC Authorization server"

**NOTES:**
1. You must use **Java 8**.
2. You need Gitlab's username/password in order to download source code and docker image.

See examples in 'abac-docker' repository for usage. 

---