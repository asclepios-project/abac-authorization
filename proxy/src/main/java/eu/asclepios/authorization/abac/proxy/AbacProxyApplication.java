/*
 * Copyright (C) 2017-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.asclepios.authorization.abac.proxy;

import eu.asclepios.authorization.abac.client.WebAccessRequestHelper;
import eu.asclepios.authorization.abac.util.properties.AuthorizationServiceClientProperties;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;

@Slf4j
@EnableZuulProxy
@SpringBootApplication(scanBasePackages = "eu.asclepios.authorization.abac.util.properties")
@RequiredArgsConstructor
public class AbacProxyApplication {

	private final AuthorizationServiceClientProperties authorizationServiceClientProperties;
	@Value("${proxy.rewrite.redirect_url:false}")
	private boolean proxyRewriteRedirectUrl;

	public static void main(String[] args) {
		SpringApplication.run(AbacProxyApplication.class, args);
	}

	@Bean
	public AbacZuulAuthorizationFilter abacZuulFilter() {
		WebAccessRequestHelper.getSingleton(authorizationServiceClientProperties);
		return new AbacZuulAuthorizationFilter();
	}

	@Bean
	public RedirectUrlRewriteFilter redirectUrlRewriteFilter() {
		log.info("Filters: 'redirect_url' rewrite: {}", proxyRewriteRedirectUrl?"enabled":"disabled");
		return proxyRewriteRedirectUrl
				? new RedirectUrlRewriteFilter() : null;
	}

	@Bean
	public HostnameRoutingFilter hostnameRoutingFilter() {
		return new HostnameRoutingFilter();
	}

	@Bean
	public AbacZuulErrorFilter abacZuulErrorFilter() {
		return new AbacZuulErrorFilter();
	}
}
