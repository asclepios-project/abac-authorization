/*
 * Copyright (C) 2017-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.asclepios.authorization.abac.proxy;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import eu.asclepios.authorization.abac.client.AccessDeniedException;
import eu.asclepios.authorization.abac.client.WebAccessRequestHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.ReflectionUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
public class AbacZuulAuthorizationFilter extends ZuulFilter {

    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 1;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() {
        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();

        log.info(String.format("%s request to %s", request.getMethod(), request.getRequestURL().toString()));

        try {
            String subject = String.format("%s:%d [%s]", request.getRemoteHost(), request.getRemotePort(), request.getRemoteUser());
            if (subject.startsWith(":")) subject = request.getRemoteAddr() + subject;
            String verb = request.getMethod();
            String object = request.getRequestURL().toString();
            WebAccessRequestHelper.getSingleton().checkWebAccessFromRequest(request, object, verb, subject, null, true);
        } catch (AccessDeniedException e) {
            log.error("ACCESS DENIED: {}", e.getMessage());
            ctx.set("error.status_code", HttpServletResponse.SC_UNAUTHORIZED);
            ctx.set("error.message", e.getMessage());
            ctx.set("error.exception", e);
            //throw new ZuulException(e, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
            ReflectionUtils.rethrowRuntimeException(e);
        } catch (Exception e) {
            log.error("EXCEPTION thrown: {}", e.getMessage());
            ctx.set("error.status_code", HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            ctx.set("error.message", e.getMessage());
            ctx.set("error.exception", e);
            //throw new ZuulException(e, HttpServletResponse.SC_UNAUTHORIZED, e.getMessage());
            ReflectionUtils.rethrowRuntimeException(e);
        }

        return null;
    }

}
