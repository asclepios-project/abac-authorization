/*
 * Copyright (C) 2017-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.asclepios.authorization.abac.proxy;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import static com.netflix.zuul.context.RequestContext.getCurrentContext;
import static org.springframework.cloud.netflix.zuul.filters.support.FilterConstants.ROUTE_TYPE;

// Code based on example at: https://github.com/spring-cloud/spring-cloud-netflix/issues/285#issuecomment-363357043
/**
 * For this filter to work, you need to add a route in zuul that maps to /**.
 * This route should be placed AFTER your other routes.
 */
@Slf4j
@Component
@ConfigurationProperties(prefix = "domains")
public class HostnameRoutingFilter extends ZuulFilter {

    /**
     * Map from hostname to route. Hosts not included here will use defaultRoute.,
     * The given route (value in the map/defaultRoute) will be added to the request.
     */
    @Getter @Setter
    private Map<String, String> hostToRoute = new HashMap<>();
    @Getter @Setter
    private String defaultRoute;

    @Override
    public String filterType() { return ROUTE_TYPE; }   // Originally it was PRE_TYPE

    @Override
    public int filterOrder() {
        // Apply this before the decoration filter, as suggested here:
        // https://github.com/spring-cloud/spring-cloud-netflix/issues/285#issuecomment-277235991
        //return PRE_DECORATION_FILTER_ORDER - 1;
        return 0;
    }

    @Override
    public boolean shouldFilter() {
        /*String serverName = getCurrentContext().getRequest().getServerName();
        return StringUtils.isNotBlank(serverName) && hostToRoute.containsKey(serverName);*/
        return hostToRoute!=null && hostToRoute.size()>0 && StringUtils.isNotBlank(defaultRoute);
    }

    @Override
    public Object run() {
        if (log.isDebugEnabled()) {
            try {
                RequestContext context = getCurrentContext();
                log.debug("HostnameRoutingFilter: DEBUG: --------------------------------------");
                log.debug("HostnameRoutingFilter: DEBUG: getFilterExecutionSummary(): {}", context.getFilterExecutionSummary());
                log.debug("HostnameRoutingFilter: DEBUG: getResponse(): {}", context.getResponse());
                log.debug("HostnameRoutingFilter: DEBUG: getResponseStatusCode(): {}", context.getResponseStatusCode());
                log.debug("HostnameRoutingFilter: DEBUG: getResponseBody(): {}", context.getResponseBody());
                log.debug("HostnameRoutingFilter: DEBUG: getOriginContentLength(): {}", context.getOriginContentLength());
                log.debug("HostnameRoutingFilter: DEBUG: getOriginResponseHeaders(): {}", context.getOriginResponseHeaders().stream()
                        .map(p->p.first()+"="+p.second()).collect(Collectors.toList()));
                log.debug("HostnameRoutingFilter: DEBUG: getRouteHost(): {}", context.getRouteHost());
                log.debug("HostnameRoutingFilter: DEBUG: getZuulResponseHeaders(): {}", context.getZuulResponseHeaders().stream()
                        .map(p->p.first()+"="+p.second()).collect(Collectors.toList()));
            } catch (Exception e) {
                log.error("HostnameRoutingFilter: run(): Exception while collecting debug info: ", e);
            }
        }

        log.debug("HostnameRoutingFilter: Configuration: host-to-route: {}, default-route: {}", hostToRoute, defaultRoute);        String hostname = getCurrentContext().getRequest().getServerName();
        HttpServletRequest request = getCurrentContext().getRequest();
        String newURI = hostToRoute.getOrDefault(hostname, defaultRoute);
        //request.setAttribute(WebUtils.INCLUDE_REQUEST_URI_ATTRIBUTE, newURI);
        log.debug("HostnameRoutingFilter: Filtering: server={}, uri={} -> {}", hostname, request.getRequestURI(), newURI);
        log.info("Route: {} -> {}", request.getRequestURL(), newURI);
        try {
            URL route = URI.create(newURI).toURL();
            getCurrentContext().setRouteHost(route);
        } catch (MalformedURLException e) {
            log.error("HostnameRoutingFilter: Error while building new route: {}\n", newURI, e);
        }
        return null;
    }
}