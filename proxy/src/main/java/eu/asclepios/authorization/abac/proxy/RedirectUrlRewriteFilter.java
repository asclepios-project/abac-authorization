/*
 * Copyright (C) 2017-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.asclepios.authorization.abac.proxy;

import com.netflix.util.Pair;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.zuul.filters.Route;
import org.springframework.cloud.netflix.zuul.filters.RouteLocator;
import org.springframework.cloud.netflix.zuul.filters.ZuulProperties;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.web.util.UrlPathHelper;

import java.net.URI;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import static org.springframework.cloud.netflix.zuul.filters.support.FilterConstants.POST_TYPE;
import static org.springframework.cloud.netflix.zuul.filters.support.FilterConstants.SEND_RESPONSE_FILTER_ORDER;

@Slf4j
public class RedirectUrlRewriteFilter extends ZuulFilter {
    private final UrlPathHelper urlPathHelper = new UrlPathHelper();

    @Autowired
    private RouteLocator routeLocator;

    @Autowired
    private ZuulProperties zuulProperties;

    private static final String LOCATION_HEADER = "Location";

    public RedirectUrlRewriteFilter() {
    }

    public RedirectUrlRewriteFilter(ZuulProperties zuulProperties,
                                    RouteLocator routeLocator) {
        this.routeLocator = routeLocator;
        this.zuulProperties = zuulProperties;
    }

    @Override
    public String filterType() {
        return POST_TYPE;
    }

    @Override
    public int filterOrder() {
        return SEND_RESPONSE_FILTER_ORDER - 1;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    @SneakyThrows
    public Object run() {
        //log.warn("---------------------------------------------------------------------");
        //log.warn("------> BEGIN");
        RequestContext ctx = RequestContext.getCurrentContext();
        //log.warn("------> ctx: {}", ctx);
        //log.warn("------> path-within-app: {}", urlPathHelper.getPathWithinApplication(ctx.getRequest()));
        Route route = routeLocator.getMatchingRoute(
                urlPathHelper.getPathWithinApplication(ctx.getRequest()));
        //log.warn("------> Route: {}", route);

        ctx.getZuulResponseHeaders().forEach(pair -> {
            if (pair != null) {
                String headerName = pair.first();
                String headerValue = pair.second();
                //log.warn("----  Zuul-Response-Header: {}: {}", headerName, headerValue);
            }
        });

        if (route != null) {
            //log.warn("------> Route != NULL");
            Pair<String, String> lh = ctx.getZuulResponseHeaders() != null
                    ? ctx.getZuulResponseHeaders().stream()
                    .filter(pair -> LOCATION_HEADER.equals(pair.first()))
                    .findFirst()
                    .orElse(null)
                    : null;
            //log.warn("------> LH: {}", lh);
            if (lh != null) {
                //log.warn("------> LH != NULL");
                String location = lh.second();
                //log.warn("------> Location: {}", location);
                URI originalRequestUri = UriComponentsBuilder
                        .fromHttpRequest(new ServletServerHttpRequest(ctx.getRequest()))
                        .build().toUri();
                //log.warn("------> originalRequestUri: {}", originalRequestUri);

                //
                int p1 = location.indexOf("&redirect_uri=");
                if (p1>0) {
                    int p2 = location.indexOf("&", p1 + 1);
                    String redirInit = p2>p1
                            ? location.substring(p1 + "&redirect_uri=".length(), p2)
                            : location.substring(p1 + "&redirect_uri=".length());
                    //log.warn("=============>  REDIR: {}", redirInit);

                    String redirDec = URLDecoder.decode(redirInit, StandardCharsets.UTF_8.name());
                    //log.warn("=============>  REDIR-DEC: {}", redirDec);
                    int p3 = redirDec.indexOf("://");
                    if (p3<0) p3 = -3;
                    int p4 = redirDec.indexOf("/", p3 + 3);
                    if (p4 > 0) redirDec = redirDec.substring(0, p4);
                    //log.warn("=============>  REDIR-DEC-BASE: {}", redirDec);

                    String fullPath = route.getFullPath();
                    //log.warn("=============>  FULL-PATH: {}", fullPath);
                    redirDec += fullPath;
                    //log.warn("=============>  NEW-REDIR-DEC: {}", redirDec);

                    String redirEnc = URLEncoder.encode(redirDec, StandardCharsets.UTF_8.name());
                    //log.warn("=============>  REDIR-ENC: {}", redirEnc);

                    location = StringUtils.replace(location, redirInit, redirEnc);
                    //log.warn("=============>  NEW-LOCATION: {}", location);

                    lh.setSecond(location);
                }
            }
            //log.warn("------> DONE Route !=NULL");
        }
        //log.warn("------> DONE");
        return null;
    }
}
