/*
 * Copyright (C) 2017-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.asclepios.authorization.abac.proxy;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import eu.asclepios.authorization.abac.client.AccessDeniedException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.util.ReflectionUtils;

import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.stream.Collectors;

@Slf4j
public class AbacZuulErrorFilter extends ZuulFilter {

    protected static final String SEND_ERROR_FILTER_RAN = "sendErrorFilter.ran";

    @Override
    public String filterType() {
        return "error";
    }

    @Override
    public int filterOrder() {
        return -1;
    }

    @Override
    public boolean shouldFilter() {
        RequestContext ctx = RequestContext.getCurrentContext();
        // only forward to errorPath if it hasn't been forwarded to already
        return ctx.getThrowable() != null
                && !ctx.getBoolean(SEND_ERROR_FILTER_RAN, false);
    }

    @Override
    public Object run() {
        try {
            RequestContext context = RequestContext.getCurrentContext();
            context.set(SEND_ERROR_FILTER_RAN);

            Throwable throwable = context.getThrowable();
            Throwable rootCause = ExceptionUtils.getRootCause(throwable);
            if (rootCause == null) rootCause = throwable;
            log.debug("AbacZuulErrorFilter: rootCause: {}", rootCause.getClass().getName());
            //boolean abacError = false;

            if (log.isDebugEnabled()) {
                try {
                    log.error("AbacZuulErrorFilter: Throwable: ", throwable);
                    log.info("AbacZuulErrorFilter: getZuulResponseHeaders(): {}", context.getZuulResponseHeaders().stream()
                            .map(p->p.first()+"="+p.second()).collect(Collectors.toList()));
                    log.info("AbacZuulErrorFilter: getOriginContentLength(): {}", context.getOriginContentLength());
                    log.info("AbacZuulErrorFilter: getOriginResponseHeaders(): {}", context.getOriginResponseHeaders().stream()
                            .map(p->p.first()+"="+p.second()).collect(Collectors.toList()));
                    log.info("AbacZuulErrorFilter: getResponseBody(): {}", context.getResponseBody());
                    log.info("AbacZuulErrorFilter: getResponseStatusCode(): {}", context.getResponseStatusCode());
                } catch (Exception e) {
                    log.error("AbacZuulErrorFilter: Exception while printing debug info: ", e);
                }
            }

            String causeMessage = rootCause.getMessage();
            String errorMessage;
            int statucCode;
            if (rootCause instanceof AccessDeniedException) {
                log.error("AbacZuulErrorFilter: Access denied: {}", causeMessage);
                context.set("error.status_code", statucCode = HttpServletResponse.SC_UNAUTHORIZED);
                context.set("error.message", causeMessage);
                errorMessage = "Access Denied: " + causeMessage;
            } else {
                log.error("AbacZuulErrorFilter: Exception thrown: {}", causeMessage);
                context.set("error.status_code", statucCode = HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                context.set("error.message", causeMessage);
                errorMessage = "Exception thrown: " + causeMessage;
            }

            HttpServletResponse servletResponse = context.getResponse();
            servletResponse.setCharacterEncoding("UTF-8");
            servletResponse.setContentType("text/plain");
            servletResponse.setStatus(statucCode);
            context.setResponseBody(errorMessage);
            context.setResponseStatusCode(statucCode);

            try (PrintWriter writer = servletResponse.getWriter()) {
                //writer.println(new JSonSerializer().toJson(rootCause));
                writer.println(errorMessage);
            }
        } catch (Exception ex) {
            log.error("Exception in AbacZuulErrorFilter.run(): ", ex);
            ReflectionUtils.rethrowRuntimeException(ex);
        }

        return null;
    }
}
