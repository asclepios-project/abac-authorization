#!/bin/sh
#
# Copyright (C) 2017-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
#
# This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
# If a copy of the MPL was not distributed with this file, You can obtain one at
# https://www.mozilla.org/en-US/MPL/2.0/
#

echo "*** ABAC Proxy ***"
echo

BASEDIR=$(dirname "$0")
CONFIG_DIR=$BASEDIR/config
echo Config. Dir.: $CONFIG_DIR
echo

# Copy sample configuration if missing
if [ ! -f "$CONFIG_DIR/application.yml" ]; then
  echo No configuration file found. Copying default configuration and keystore.
  echo Any existing files will not be altered.
  cp -n -v samples/config/* $CONFIG_DIR/
  echo
fi

# Run ABAC Proxy
export CONFIG_DIR
java -jar abac-zuul-proxy.jar --spring.config.location=file:$CONFIG_DIR/application.yml
