/*
 * Copyright (C) 2017-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.asclepios.authorization.abac.util;

import eu.asclepios.authorization.abac.util.properties.HttpClientProperties;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import java.io.File;
import java.io.FileInputStream;
import java.security.KeyStore;
import java.security.SecureRandom;

@Slf4j
public class RestClientUtil {
	
	public static RestClient createRestClient(HttpClientProperties properties, String endpoint) throws Exception {
		// if 'endpoint' starts with 'https', then use SSL, else use default http client
		boolean useSsl = StringUtils.isNotBlank(endpoint) && endpoint.trim().length() > 5 && endpoint.substring(0, 6).toLowerCase().startsWith("https:");
		return createRestClient(properties, useSsl);
	}
	
	public static RestClient createRestClient(HttpClientProperties properties, boolean useSsl) throws Exception {
		// Get http client configuration
		String trustStoreFile = properties!=null ? properties.getKeystoreFile() : null;
		String trustStoreFileType = properties!=null ? properties.getKeystoreFileType() : null;
		String trustStorePassword = properties!=null ? properties.getKeystorePassword() : null;
		String sslProtocol = properties!=null ? properties.getSslProtocol() : null;

		trustStoreFile = trustStoreFile!=null ? trustStoreFile.trim() : "";
		trustStoreFileType = trustStoreFileType!=null ? trustStoreFileType.trim() : "";
		trustStorePassword = trustStorePassword!=null ? trustStorePassword.trim() : "";
		sslProtocol = sslProtocol!=null ? sslProtocol.trim() : "";

		// if 'useSsl' is false or truststore file is empty, then we return restClient with default http client
		if (! useSsl || trustStoreFile.isEmpty()) {
			// Plain HTTP is assumed since we don't have an https PDP endpoint or a keystore file
			log.debug("** RestClient initialized with an Http client");
			return new RestClient(HttpClients.custom().build());
		}
		
		// if 'useSsl' is true then an HTTPS client will be used
		trustStoreFileType = StringUtils.isNotBlank(trustStoreFileType)
				? StringUtils.trim(trustStoreFileType) : KeyStore.getDefaultType(); //returns "JKS"
		log.info("Truststore type: {}", trustStoreFileType);
		KeyStore trustStore = KeyStore.getInstance(trustStoreFileType);
		try (FileInputStream instream = new FileInputStream(new File(trustStoreFile))) {
			log.info("Loading truststore from: {}", trustStoreFile);
			trustStore.load(instream, trustStorePassword.toCharArray());
		}

		TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
		trustManagerFactory.init(trustStore);
		KeyManager[] keyManagers = {};
		SSLContext sslContext = SSLContext.getInstance(sslProtocol);
		sslContext.init(keyManagers, trustManagerFactory.getTrustManagers(), new SecureRandom());
		//SSLContexts.custom().loadTrustMaterial(trustStore, null).build();

/*		javax.net.ssl.SSLContext sslContext =
			new SSLContextBuilder()
			//or SSLContexts.custom()
				.loadTrustMaterial(trustStore)
				//.loadTrustMaterial(trustStore, new TrustSelfSignedStrategy())
				//.loadKeyMaterial(keyStore, keyStorePassword.toCharArray())
				.build();*/

		//log.debug("Source file: "+ org.apache.http.conn.ssl.SSLConnectionSocketFactory.class.getProtectionDomain().getCodeSource().getLocation().getPath() );

		//RestClient httpClient = HttpClients.custom().setSSLSocketFactory(socketFactory).build();
		//HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory(httpClient);

		CloseableHttpClient httpClient = HttpClients.custom()
				//.setSSLContext(sslContext)
				//.setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE)	// allow all hosts  (see also: BrowserCompatHostnameVerifier, DefaultHostnameVerifier)
				.setSSLSocketFactory(new SSLConnectionSocketFactory(sslContext, NoopHostnameVerifier.INSTANCE))
				.build();

		log.debug("** RestClient initialized with HTTPS");
		return new RestClient(httpClient);
	}

}