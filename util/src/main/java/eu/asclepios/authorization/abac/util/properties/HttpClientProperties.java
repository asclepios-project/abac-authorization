/*
 * Copyright (C) 2017-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.asclepios.authorization.abac.util.properties;

import lombok.Data;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;

@Data
@ToString(exclude="keystorePassword")
@Configuration
@Slf4j
public class HttpClientProperties {
	private String keystoreFile;
	private String keystoreFileType = "PKCS12";
	private String keystorePassword;
	private String sslProtocol = "TLSv1.2";
}
