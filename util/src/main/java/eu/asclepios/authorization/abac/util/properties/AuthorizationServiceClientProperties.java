/*
 * Copyright (C) 2017-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.asclepios.authorization.abac.util.properties;

import lombok.Data;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.List;

@Data
@ToString(exclude="jwtSecret")
@Validated
@Configuration
@ConfigurationProperties
@PropertySource("file:${CONFIG_DIR}/authorization-client.properties")
@Slf4j
public class AuthorizationServiceClientProperties {
	
	@NotNull
	private PdpConfig pdp;

	private DataProperties data;

	private ContextClientProperties context;

	private String jwtSecret;
	private boolean contentCollectionEnabled = true;
	private List<String> contentTypesCollected = Collections.singletonList("application/json");
	private int contentLengthLimit = 1024*1024;

	// --------------------------------------------------------------
	
	@Data
	@ToString(exclude="accessKey")
	public static class PdpConfig {
		private boolean disabled;
		@NotNull
		private String[] endpoints;
		private String loadBalanceMethod;
		private int retryCount;
		@NotBlank
		private String accessKey;
		@NotNull
		private HttpClientProperties httpClient;
	}
	
	// --------------------------------------------------------------
	
	@Data
	public static class DataProperties {
		private String[] extractors;
	}
	
	// --------------------------------------------------------------
	
	public static boolean booleanValue(String str) {
		return booleanValue(str, false);
	}
	
	public static boolean booleanValue(String str, boolean defVal) {
		if (str==null || (str=str.trim()).isEmpty()) return defVal;
		return (str.equalsIgnoreCase("true") || str.equalsIgnoreCase("yes") || str.equalsIgnoreCase("on"));
	}
}
