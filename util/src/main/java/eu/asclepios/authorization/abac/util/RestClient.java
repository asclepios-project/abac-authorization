/*
 * Copyright (C) 2017-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.asclepios.authorization.abac.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.*;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;

import javax.validation.constraints.NotNull;

public class RestClient {
    private CloseableHttpClient httpClient;
    private ObjectMapper objectMapper;

    RestClient(@NotNull CloseableHttpClient httpClient) {
        this.httpClient = httpClient;
        this.objectMapper = new ObjectMapper();
    }

    @SneakyThrows
    public String get(String url) { return get(url, String.class); }

    @SneakyThrows
    public <T>T get(String url, Class<T> responseType) { return get(url, responseType, null); }

    @SneakyThrows
    public <T>T get(String url, Class<T> responseType, String apiKey) {
        HttpRequestBase request = prepareHeaders(new HttpGet(url), apiKey);
        HttpResponse response = httpClient.execute(request);
        return responseToObject(response, responseType);
    }

    @SneakyThrows
    public String post(String url, String requestBody) { return post(url, requestBody, String.class); }

    @SneakyThrows
    public String post(String url, String requestBody, String apiKey) { return post(url, requestBody, String.class, apiKey); }

    @SneakyThrows
    public <T>T post(String url, Object requestBody, Class<T> responseType) { return post(url, requestBody, responseType, null); }

    @SneakyThrows
    public <T>T post(String url, Object requestBody, Class<T> responseType, String apiKey) {
        HttpPost request = (HttpPost) prepareHeaders(new HttpPost(url), apiKey);
        objectToRequestBody(request, requestBody);
        HttpResponse response = httpClient.execute(request);
        return responseToObject(response, responseType);
    }

    @SneakyThrows
    public String put(String url, String requestBody) { return put(url, requestBody, String.class, null); }

    @SneakyThrows
    public String put(String url, String requestBody, String apiKey) { return put(url, requestBody, String.class, apiKey); }

    @SneakyThrows
    public <T>T put(String url, Object requestBody, Class<T> responseType) { return put(url, requestBody, responseType, null); }

    @SneakyThrows
    public <T>T put(String url, Object requestBody, Class<T> responseType, String apiKey) {
        HttpPut request = (HttpPut) prepareHeaders(new HttpPut(url), apiKey);
        objectToRequestBody(request, requestBody);
        HttpResponse response = httpClient.execute(request);
        return responseToObject(response, responseType);
    }

    @SneakyThrows
    public String delete(String url) { return delete(url, null); }

    @SneakyThrows
    public String delete(String url, String apiKey) {
        HttpRequestBase request = prepareHeaders(new HttpDelete(url), apiKey);
        HttpResponse response = httpClient.execute(request);
        return EntityUtils.toString(response.getEntity());
    }

    // ------------------------------------------------------------------------

    @SneakyThrows
    private <T>T responseToObject(HttpResponse response, Class<T> responseType) {
        //String json = EntityUtils.toString(response.getEntity());
        //return objectMapper.readerFor(responseType).readValue(json);
        if (String.class.isAssignableFrom(responseType))
            return (T) EntityUtils.toString(response.getEntity());
        return objectMapper.readerFor(responseType).readValue(response.getEntity().getContent());
    }

    @SneakyThrows
    private HttpEntityEnclosingRequestBase objectToRequestBody(HttpEntityEnclosingRequestBase request, Object requestBody) {
        String jsonBody = objectToJson(requestBody);
        request.setEntity(new StringEntity(jsonBody));
        return request;
    }

    @SneakyThrows
    private String objectToJson(Object obj) {
        String json = "";
        if (obj!=null) {
            json = (obj instanceof String)
                    ? obj.toString()
                    : objectMapper.writeValueAsString(obj);
        }
        return json;
    }

    private HttpRequestBase prepareHeaders(HttpRequestBase request, String apiKey) {
        request.addHeader("content-type", "application/json");
        if (StringUtils.isNotBlank(apiKey)) request.addHeader("X-API-KEY", apiKey);
        return request;
    }
}
