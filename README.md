<!--
  ~
  ~ Copyright (C) 2017-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
  ~
  ~ This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
  ~ If a copy of the MPL was not distributed with this file, You can obtain one at
  ~ https://www.mozilla.org/en-US/MPL/2.0/
  ~
  -->
# ASCLEPIOS ABAC Authorization

*ASCLEPIOS ABAC Authorization* project offers the tools needed to enhance an application
with *Attribute-Based Access Control* authorization. It particularly follows the paradigm
of *XACML Architecture*, which is the de facto standard for ABAC authorization.
ASCLEPIOS ABAC Authorization has been implemented using Client-Server approach:

- ABAC client (provided as a library) is wired to the HTTP request processing flow 
  of the protected application (your application).
  This way ABAC client is able to intercept incoming requests, extract the needed 
  information, and ask ABAC server if it is ok to let request processing continue. 
  ABAC client implements the *Policy Enforcement Point (PEP)* in XACML Architecture.
- ABAC server is a standalone service, which accepts authorization requests from 
  ABAC clients, and decide if they can be allowed on not, based on a number of XACML 
  policies. ABAC server is the *Policy Decision Point (PDP)* in XACML Architecture.

#### Modules
ASCLEPIOS ABAC Authorization project comprises the following modules:

- *client*: provides the ABAC Client library. For information on building and using 
  ABAC client library refer to the module's [README][1] file.
- *client-tomcat*: provides the ABAC Client library packaged for use with standalone 
  Tomcat servers.
- *context*: is an internal service of ABAC server and clients used for acquiring 
  additional contextual information.
- *proxy*: provides a reverse proxy capable for Keycloak-based authentication and 
  ABAC authorization. For information on using the ABAC proxy refer to the module's
  [README][3] file.
- *server*: provides the ABAC Server application. For information on building and 
  using ABAC client refer to the module's [README][2] file.
- *util*: provides auxiliary functionality used by other modules.

[1]: client/README.md "ABAC Authorization Client library"
[2]: server/README.md "ABAC Authorization Server"
[3]: proxy/README.md "ABAC Zuul Proxy"

#### License
ASCLEPIOS ABAC Authorization is licensed with *Mozilla Public License Version 2.0*.
For more information check [LICENSE][3] file.

[3]: LICENSE "License"

---