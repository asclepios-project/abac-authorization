<!--
  ~
  ~ Copyright (C) 2017-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
  ~
  ~ This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
  ~ If a copy of the MPL was not distributed with this file, You can obtain one at
  ~ https://www.mozilla.org/en-US/MPL/2.0/
  ~
  -->
# ASCLEPIOS ABAC Authorization Server (PDP)

ABAC server is a standalone service that accepts authorization requests and 
decides if they can be allowed on not, based on a number of XACML policies.
ABAC server is a Policy Decision Point (PDP) according to XACML Architecture.
It can be installed and launched as a standalone (web) service.

[AMPLE policy editor][1] can be used to create/modify policies, and publish them to ABAC Server.

[1]: https://gitlab.com/asclepios-project/ample-editor "AMPLE policy editor"

**NOTES:**
1. You must use **Java 8**.
2. You need Gitlab's username/password in order to download source code and docker image.

---
## Build ABAC Server

#### Build from sources
1. Clone the project from Gitlab:
    ```
    git clone https://gitlab.com/asclepios-project/abac-authorization.git
    ```
2. Build the project:
    ```
    cd abac-authorization
    mvn clean install
   ```
3. *[OPTIONAL]* Add a sample, *permit-all* policy:

    In Linux 
    ```
    cp src/main/resources/policies/examples/permit-ALL-policy.xml src/main/resources/policies
    ```
    In Windows
    ```
    copy src\main\resources\policies\examples\permit-ALL-policy.xml src\main\resources\policies
    ```
   
#### Build the Docker image

Follow previous section's instructions and add **-P with-docker** during Maven build, i.e.
```
mvn clean install -P with-docker
```

The new Docker image will be named `asclepios/abac-authorization-server:latest`.

---
## [IMPORTANT] Server Security

ABAC Server communications are encrypted using TLS (HTTPS) and for this reason 
a private key-certificate pair is required. ABAC Server is bundled with a default,
self-signed pair, stored in `pdp-server-keystore.p12` file.
A truststore file (`pdp-server-truststore.p12`) containing only the certificate,
is also provided for use in applications needing to  contact ABAC server 
(like the ABAC clients). Both keystore and truststore files are protected with
a default password.

Moreover, ABAC Server uses an API Key in order to authenticate the legitimate clients.
A default API Key value has been used in default configuration of ABAC Server and Client.

**CAUTION**

The keystore/truststore files, their password, and the API Key included in default 
configurations are provided for testing and server as examples.
**They must be changed in production.** 

#### New self-signed Key-Certificate pair

Self-signed certificates are cheap and easy to generate but have limitations and
require additional configuration at client-side. If you expect that a lot of ABAC 
clients will use the ABAC server or if there are limitations on sharing truststore
file then consider using a Key-Certificate pair from a well-known Certification
Authority (CA), like [Let's Encrypt][2] (it is free).

[2]: https://letsencrypt.org/ "Let's Encrypt"

Using the following commands you can create new keystore and truststore files including
a new self-signed key-certificate pair. **Please don't use the default password** for
protecting keystore and truststore files. You can also use a different password for each.
These instructions use `keytool` program included in standard Java SDK installations.

    keytool -genkey -alias pdp-server -keyalg RSA -keysize 2048 -storetype PKCS12 -storepass <YOUR_KEYSTORE_PASSWORD> -dname "CN=pdp-server,OU=Information Management Unit (IMU),O=Institute of Communication and Computer Systems (ICCS),L=Athens,ST=Attika,C=GR" -ext "SAN=dns:pdp-server,dns:localhost,ip:127.0.0.1" -keystore pdp-server-keystore.p12 -startdate -1d -validity 3650
    
    keytool -list -v -storetype pkcs12 -keystore pdp-server-keystore.p12
    
    keytool -export -storetype PKCS12 -keystore pdp-server-keystore.p12 -storepass <YOUR_KEYSTORE_PASSWORD> -alias pdp-server -file pdp-server.crt
    
    keytool -printcert -file pdp-server.crt
    
    keytool -import -alias pdp-server -file pdp-server.crt -storetype PKCS12 -keystore pdp-server-truststore.p12 -storepass <YOUR_TRUST_STORE_PASSWORD>
    Trust this certificate? [no]:  yes
    
    keytool -list -v -storetype pkcs12 -keystore pdp-server-truststore.p12

Keystore and truststore file names can be changed but ABAC server and ABAC client
configurations must be updated accordingly. 

- For ABAC Server, edit `authorization-server.properties` file and set properties `server.ssl.key-store`,
  `server.ssl.key-store-password`, `server.ssl.keyStoreType`, and `server.ssl.keyAlias`
- For ABAC Client, edit `authorization-client.properties` file and set properties `pdp.http-client.keystore-file`,
  `pdp.http-client.keystore-file-type`, and `pdp.http-client.keystore-password`
  or change the corresponding environment variables `AZ_CLIENT_TRUST_STORE_FILE`, `AZ_CLIENT_TRUST_STORE_TYPE`, 
  and `AZ_CLIENT_TRUST_STORE_PASSWORD`. 
  See ABAC Client [README][3] file for details.

#### New API Key

Create a long, difficult to guess (random) string. We suggest more than 32 characters long,
including any combination of capital and plain letters, numbers and symbols.
Avoid using phrases or values that can be guessed or extracted from context.

New API Key value must be set in both ABAC Server and ABAC Client configuration files.

- For ABAC Server, edit `authorization-server.properties` file and set property `pdp.access-key`.
- For ABAC Client, edit `authorization-client.properties` file and set property `pdp.access-key`
  or change the corresponding environment variable `AZ_SERVER_ACCESS_KEY`.
  See ABAC Client [README][3] file for details.

[3]: ../client/README.md "ABAC Authorization Client library"

---
## Run ABAC Server

#### Run with Docker image

*Requirements:*
- Requires docker being installed and working in VM or computer.
- The first time you will need to login to Gitlab's docker repository to retrieve the image.

*Quick start*

The next instructions will download ABAC Server Docker image and start 
it up using default configuration, and a sample, permit-all policy.
This is good just for testing the Docker image works.

    docker login registry.gitlab.com
    ....provide your Gitlab credentials....
    docker run --rm -it -p 7071:7071 registry.gitlab.com/asclepios-project/abac-authorization/abac-server

Visit https://localhost:7071/ using a browser.

*Proper installation and start*

This is the recommended way to install and start ABAC Server with Docker.

1. Create folders in host machine/VM for storing ABAC server's configuration, logs and policies.
   Let's assume these folders will be located at *'~/abac'* folder.

	`mkdir -p ~/abac/conf ~/abac/policies ~/abac/logs`

2.  Run ABAC server using Docker image. The first time you will need to login to project's Docker repository.
    ```
    docker login registry.gitlab.com
    ....provide your Gitlab credentials....
    docker run --rm -it -p 7071:7071 -v ~/abac/conf:/abac-server/config -v ~/abac/policies:/abac-server/policies -v ~/abac/logs:/abac-server/logs registry.gitlab.com/asclepios-project/abac-authorization/abac-server
    ```

3. When ABAC server starts it will populate `conf` and `policies` folders with default configuration and a sample policy respectively.
   You can modify them and restart container.

---
#### Run without Docker image

*Requirements:*
- Requires a local copy of the whole ABAC Authorization project (see section *Build from sources*)

In Linux:

	cd abac-authorization-service/server
	CONFIG_DIR=src/main/resources/config
	export CONFIG_DIR
	java -jar target/abac-authorization-server.jar

or `abac-authorization-service/server/bin/run.sh`
    
In Windows:

	cd abac-authorization-service\server
	set CONFIG_DIR=src\main\resources\config
	java -jar target\abac-authorization-server.jar

or `abac-authorization-service\server\bin\run.bat`

---