@echo off
::
:: Copyright (C) 2017-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
::
:: This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
:: If a copy of the MPL was not distributed with this file, You can obtain one at
:: https://www.mozilla.org/en-US/MPL/2.0/
::

set BASEDIR=%~dp0..
set CONFIG_DIR=%BASEDIR%\src\main\resources\config
if "%1"=="" (
	set SERVER_PORT=7071
) else (
	set SERVER_PORT=%1
)
::java -jar %BASEDIR%\target\abac-authorization-server-exec.jar --spring.config.location=file:%CONFIG_DIR%\authorization-server.properties --server.port=%SERVER_PORT%

if not "%ABAC_SERVER_LIB_PATH%"=="" (
    set LOADER_PATH=-Dloader.path=%ABAC_SERVER_LIB_PATH%
    echo   External Lib. path: %ABAC_SERVER_LIB_PATH%
)
java -cp %BASEDIR%\target\abac-authorization-server-exec.jar  %LOADER_PATH%  org.springframework.boot.loader.PropertiesLauncher --spring.config.location=file:%CONFIG_DIR%\authorization-server.properties --server.port=%SERVER_PORT%
