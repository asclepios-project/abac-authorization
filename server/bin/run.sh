#!/bin/bash
#
# Copyright (C) 2017-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
#
# This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
# If a copy of the MPL was not distributed with this file, You can obtain one at
# https://www.mozilla.org/en-US/MPL/2.0/
#

BASEDIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd .. && pwd )
if [[ -z "$1" ]]; then
    CONFIG_DIR=$BASEDIR
    if [[ -d "$BASEDIR/src/main/resources/config" ]]; then CONFIG_DIR=$BASEDIR/src/main/resources/config; fi
    if [[ -d "$BASEDIR/config" ]]; then CONFIG_DIR=$BASEDIR/config; fi
    if [[ -d "$BASEDIR/conf" ]]; then CONFIG_DIR=$BASEDIR/conf; fi
else
    CONFIG_DIR=$1
fi
echo Configuration folder: $CONFIG_DIR
export CONFIG_DIR
#java -jar $BASEDIR/target/abac-authorization-server-exec.jar --spring.config.location=file:$CONFIG_DIR/authorization-server.properties

if [[ ! -z "$ABAC_SERVER_LIB_PATH" ]]; then
    LOADER_PATH=-Dloader.path=$ABAC_SERVER_LIB_PATH
    echo External Lib. path: $ABAC_SERVER_LIB_PATH
fi
java -cp $BASEDIR/target/abac-authorization-server-exec.jar  $LOADER_PATH  org.springframework.boot.loader.PropertiesLauncher --spring.config.location=file:$CONFIG_DIR/authorization-server.properties
