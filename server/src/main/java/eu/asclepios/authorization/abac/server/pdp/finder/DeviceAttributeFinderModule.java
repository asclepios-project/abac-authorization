/*
 * Copyright (C) 2017-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.asclepios.authorization.abac.server.pdp.finder;

import au.com.flyingkite.mobiledetect.UAgentInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.wso2.balana.XACMLConstants;
import org.wso2.balana.cond.EvaluationResult;
import org.wso2.balana.ctx.EvaluationCtx;
import org.wso2.balana.utils.Constants.PolicyConstants;

import java.net.URI;

/**
 * Geolocation Attribute Finder
 */
@Slf4j
public class DeviceAttributeFinderModule extends BaseAttributeFinderModule {

	public final static String DEVICE_TYPE = "device-type";
	public final static URI USER_AGENT_URI = URI.create("http-header-user-agent");

	@Override
	public boolean isDesignatorSupported() {
		return true;
	}

	@Override
	public EvaluationResult findAttribute(URI attributeType, URI attributeId,
										  String issuer, URI category, EvaluationCtx context)
	{
		log.debug("Arguments: attr-type: {}, attr-id: {}, issuer: {}, category: {}, context: {}",
				attributeType, attributeId, issuer, category, context);

		if (attributeMatches(
				PolicyConstants.STRING_DATA_TYPE, attributeType,
				DEVICE_TYPE, attributeId,
				XACMLConstants.ENT_CATEGORY, category))
		{
			// Get User-Agent from HTTP request headers (if included in XACML request)
			log.debug("--------    Searching for User-Agent...");
			String userAgent = findStringAttribute(attributeType, USER_AGENT_URI, issuer, category, context);
			log.debug("--------    Found User-Agent: {}", userAgent);
			if (StringUtils.isBlank(userAgent)) {
				return createEmptyStringResult();
			}

			// Lookup location using IP address
			UAgentInfo agentInfo = new UAgentInfo(userAgent, null);
			boolean isMobile = agentInfo.detectMobileQuick();
			boolean isTablet = agentInfo.detectTierTablet();
			boolean isDesktop = !(isMobile || isTablet);
			log.debug("--------    Agent info: mobile={}, tablet={}, desktop={}", isMobile, isTablet, isDesktop);
			String deviceType = isTablet ? "Tablet" :
					isMobile ? "Mobile" : "Desktop";
			log.debug("--------    Device type: {}", deviceType);

			return createStringResult(category, attributeType, attributeId, deviceType, context);
		}

		return createEmptyResult(attributeType);
	}
}
