/*
 * Copyright (C) 2017-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.asclepios.authorization.abac.server.pap;

import lombok.Data;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.regex.Pattern;

@Slf4j
@Data
@Validated
@Configuration
@ConfigurationProperties(prefix="pap")
public class PapProperties implements InitializingBean {
    private String apiKey;
    @NotBlank
    private String abacPoliciesDirectory;
    private String abacPolicyFileNamePattern = "${POLICY_ID}.xacml.xml";
    private Pattern abacPolicyIdPattern;
    @NotBlank
    private String abePoliciesDirectory;
    private String abePolicyFileNamePattern = "${POLICY_ID}.txt";
    private Pattern abePolicyIdPattern;

    @Override
    public void afterPropertiesSet() {
        _checkDir(getAbacPoliciesDirectory(), "ABAC Policies");
        _checkDir(getAbePoliciesDirectory(), "ABE Policies");
    }

    protected void _checkDir(String dir, String policyTypeName) {
        Path policiesDirectory = Paths.get(dir);
        if (!policiesDirectory.toFile().exists())
            throw new RuntimeException("Configuration Error: "+policyTypeName+" directory does not exist: "+policiesDirectory);
        log.info("{} directory: {}", policyTypeName, policiesDirectory);
    }
}
