/*
 * Copyright (C) 2017-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.asclepios.authorization.abac.server.pdp.finder;

import inet.ipaddr.IPAddressString;
import inet.ipaddr.ipv6.IPv6Address;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.wso2.balana.XACMLConstants;
import org.wso2.balana.cond.EvaluationResult;
import org.wso2.balana.ctx.EvaluationCtx;
import org.wso2.balana.utils.Constants.PolicyConstants;

import java.net.URI;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Geolocation Attribute Finder Module
 * Uses the IP-API geolocation service, https://ip-api.com/docs
 */
@Slf4j
public class GeolocationAttributeFinderModule extends BaseAttributeFinderModule {

	public final static String IP_GEOLOCATION_URL = "http://ip-api.com/json/%s?fields=status,message,continent,continentCode,country,countryCode,region,regionName,city,district,zip,lat,lon,timezone,offset,currency,isp,org,mobile,proxy,hosting,query";
	public final static URI IP_ADDRESS_ATTRIBUTE_URI = URI.create("http-req-remote-address");
	public final static boolean CONVERT_IPv6_TO_IPv4 = true;

	public final static Map<String,String> ATTRIBUTE_TO_FIELD_MAP = Collections.unmodifiableMap(
			new HashMap<String,String>() {{
				put("location-continent", "continent");
				put("location-continent-code", "continentCode");
				put("location-country", "country");
				put("location-country-code", "countryCode");
				put("location-region", "region");
				put("location-region-name", "regionName");
				put("location-city", "city");
				put("location-district", "district");
				put("location-zip", "zip");
				put("location-lat", "lat");
				put("location-lon", "lon");
				put("location-timezone", "timezone");
				put("location-offset", "offset");
				put("location-currency", "currency");
				put("location-isp", "isp");
				put("location-org", "org");
				put("location-mobile", "mobile");
				put("location-proxy", "proxy");
				put("location-hosting", "hosting");
			}}
	);

	public final static RestTemplate restClient = new RestTemplate();

	@Override
	public boolean isDesignatorSupported() {
		return true;
	}

	@Override
	public EvaluationResult findAttribute(URI attributeType, URI attributeId,
										  String issuer, URI category, EvaluationCtx context)
	{
		log.debug("Arguments: attr-type: {}, attr-id: {}, issuer: {}, category: {}, context: {}",
				attributeType, attributeId, issuer, category, context);

		String attrId = attributeId.toASCIIString();
		if (!ATTRIBUTE_TO_FIELD_MAP.containsKey(attrId)) {
			log.debug("--------    Attribute not supported: {}", attrId);
			return createEmptyResult(attributeType);
		}

		if (attributeMatches(
				PolicyConstants.STRING_DATA_TYPE, attributeType,
				attrId, attributeId,
				XACMLConstants.ENT_CATEGORY, category))
		{
			// Get remote IP address from HTTP request (if included in XACML request)
			log.debug("--------    Searching for request IP address...");
			String ipAddr = findStringAttribute(attributeType, IP_ADDRESS_ATTRIBUTE_URI, issuer, category, context);
			log.debug("--------    Found IP address: {}", ipAddr);
			if (StringUtils.isBlank(ipAddr)) {
				return createEmptyStringResult();
			}

			// Convert IPv6 address to IPv4, if it is an IPv6 address and it is convertible
			if (CONVERT_IPv6_TO_IPv4) {
				IPAddressString ipAddrStr = new IPAddressString(ipAddr);
				if (ipAddrStr.isIPv6()) {
					log.debug("--------    IPv6 address found: {}", ipAddr);
					IPv6Address ipv6 = ipAddrStr.getAddress().toIPv6();
					if (ipv6.isIPv4Convertible()) {
						ipAddr = ipv6.toIPv4().toNormalizedString();
						log.debug("--------    Converted to IPv4 address: {}", ipAddr);
					} else if (ipv6.isLocal() || ipv6.isLoopback()) {
						ipAddr = "127.0.0.1";
						log.debug("--------    IP address set to local: {}", ipAddr);
					} else
						log.debug("--------    IPv6 address is NOT convertible: {}", ipAddr);
				} else {
					log.debug("--------    IPv4 address found: {}", ipAddr);
				}
			}

			// Lookup location using IP address
			String value = geolocationLookup(ipAddr, attributeId.toASCIIString());
			if (StringUtils.isNotBlank(value)) {
				return createStringResult(category, attributeType, attributeId, value, context);
			}
		}

		return createEmptyResult(attributeType);
	}

	protected String geolocationLookup(String ipAddr, String attributeId) {
		String geolocationField = ATTRIBUTE_TO_FIELD_MAP.get(attributeId);
		log.debug("--------    Geolocation lookup params: ip-address={}, attr-id={}, field={}",
				ipAddr, attributeId, geolocationField);
		if (geolocationField==null)
			return null;
		String url = String.format(IP_GEOLOCATION_URL, ipAddr);
		log.debug("--------    Geolocation URL: {}", url);
		ResponseEntity<Map> response = restClient.getForEntity(URI.create(url), Map.class);
		if (response.getStatusCode().is2xxSuccessful()) {
			Map results = response.getBody();
			log.debug("--------    Geolocation results: {}", results);
			if (results!=null) {
				if ("success".equalsIgnoreCase(results.getOrDefault("status", "fail").toString())) {
					Object valueObj = results.get(geolocationField);
					if (valueObj!=null) {
						String value = valueObj.toString();
						log.debug("--------    Geolocation results: {}={}", geolocationField, value);
						return value;
					}
				}
			}
		} else {
			log.debug("--------    Geolocation lookup failed: status={}", response.getStatusCode());
		}
		return null;
	}
}
