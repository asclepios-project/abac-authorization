/*
 * Copyright (C) 2017-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.asclepios.authorization.abac.server.pdp.finder;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.keycloak.representations.IDToken;
import org.wso2.balana.XACMLConstants;
import org.wso2.balana.cond.EvaluationResult;
import org.wso2.balana.ctx.EvaluationCtx;
import org.wso2.balana.utils.Constants.PolicyConstants;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.URI;
import java.util.Base64;

/**
 * Keycloack Attribute Finder
 */
@Slf4j
public class KeycloackAttributeFinderModule extends BaseAttributeFinderModule {

	public final static String KC_PREFIX = "kc-";
	public final static URI AUTHORIZATION_TOKEN_URI = URI.create("kc-id-token");

	@Override
	public boolean isDesignatorSupported() {
		return true;
	}

	@Override
	public EvaluationResult findAttribute(URI attributeType, URI attributeId,
										  String issuer, URI category, EvaluationCtx context)
	{
		log.debug("Arguments: attr-type: {}, attr-id: {}, issuer: {}, category: {}, context: {}",
				attributeType, attributeId, issuer, category, context);

		if (attributeId!=null && attributeType!=null && category!=null
				&& attributeId.toASCIIString().startsWith(KC_PREFIX)
				&& PolicyConstants.STRING_DATA_TYPE.equals(attributeType.toASCIIString())
				&& XACMLConstants.ENT_CATEGORY.equals(category.toASCIIString()))
		{
			// Guard to prevent recursion when searching for AUTHORIZATION_TOKEN_URI attribute
			if (AUTHORIZATION_TOKEN_URI.equals(attributeId)) {
				log.warn("Recursion detected: Will return immediately: Arguments: attr-type: {}, attr-id: {}, issuer: {}, category: {}, context: {}",
						attributeType, attributeId, issuer, category, context);
				return createEmptyResult(attributeType);
			}

			// Get Keycloak ID Token from XACML request
			log.debug("--------    Searching for Keycloak IDToken...");
			String idTokenStr = findStringAttribute(attributeType, AUTHORIZATION_TOKEN_URI, issuer, category, context);
			if (StringUtils.isBlank(idTokenStr)) {
				log.debug("--------    Not found Keycloak IDToken");
				return createEmptyStringResult();
			}
			log.debug("--------    Found Keycloak IDToken: {}", idTokenStr);

			// Deserialize ID Token
			IDToken idToken = null;
			try {
				byte[] idTokenBytes = Base64.getDecoder().decode(idTokenStr);
				ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(idTokenBytes));
				idToken = (IDToken)ois.readObject();
				log.debug("--------    Unserialized Keycloak IDToken: {}", idToken);

				// Analyze ID Token
				String customClaim = attributeId.toASCIIString().substring(KC_PREFIX.length());
				log.debug("--------    Retrieving custom claim from Keycloak IDToken: {}", customClaim);
				Object claimValue = idToken.getOtherClaims().get(customClaim);
				if (claimValue!=null) {
					log.debug("--------    Found custom claim from Keycloak IDToken: {} = {}", customClaim, claimValue);
					return createStringResult(category, attributeType, attributeId, claimValue.toString(), context);
				}
				log.debug("--------    Not found custom claim from Keycloak IDToken: {}", customClaim);

			} catch (IOException | ClassNotFoundException e) {
				log.error("--------    Deserialization of ID Token failed: ", e);
			}
		}

		return createEmptyResult(attributeType);
	}
}
