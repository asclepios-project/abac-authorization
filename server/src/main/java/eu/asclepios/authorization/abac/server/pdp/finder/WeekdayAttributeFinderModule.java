/*
 * Copyright (C) 2017-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.asclepios.authorization.abac.server.pdp.finder;

import lombok.extern.slf4j.Slf4j;
import org.wso2.balana.XACMLConstants;
import org.wso2.balana.cond.EvaluationResult;
import org.wso2.balana.ctx.EvaluationCtx;
import org.wso2.balana.utils.Constants.PolicyConstants;

import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

/**
 * Weekday Attribute Finder
 */
@Slf4j
public class WeekdayAttributeFinderModule extends BaseAttributeFinderModule {

	public final static String ENVIRONMENT_CURRENT_WEEKDAY = "current-weekday";

	private final SimpleDateFormat dateFormat;

	public WeekdayAttributeFinderModule() {
		dateFormat = new SimpleDateFormat("EEEE", Locale.ENGLISH);
	}

	@Override
	public boolean isDesignatorSupported() {
		return true;
	}

	@Override
	public Set<String> getSupportedCategories() {
		HashSet<String> set = new HashSet<>();
		set.add(XACMLConstants.ENT_CATEGORY);
		return set;
	}

	public void close() {
	}
	
	@Override
	public EvaluationResult findAttribute(URI attributeType, URI attributeId, 
			String issuer, URI category, EvaluationCtx context) 
	{
		log.debug("Arguments: attr-type: {}, attr-id: {}, issuer: {}, category: {}, context: {}",
				attributeType, attributeId, issuer, category, context);

		if (attributeMatches(
				PolicyConstants.STRING_DATA_TYPE, attributeType,
				ENVIRONMENT_CURRENT_WEEKDAY, attributeId,
				XACMLConstants.ENT_CATEGORY, category))
		{
			// get attribute value
			String weekday = dateFormat.format(new Date());
			log.debug("Attribute Value: {}", weekday);

			// create and log the result
			return createStringResult(category, attributeType, attributeId, weekday, context);
		}

		return createEmptyResult(attributeType);
	}
}