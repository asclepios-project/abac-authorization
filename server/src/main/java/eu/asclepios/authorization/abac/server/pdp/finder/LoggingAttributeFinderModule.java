/*
 * Copyright (C) 2017-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.asclepios.authorization.abac.server.pdp.finder;

import lombok.extern.slf4j.Slf4j;
import org.wso2.balana.cond.EvaluationResult;
import org.wso2.balana.ctx.EvaluationCtx;

import java.net.URI;

/**
 * Logging Attribute Finder
 */
@Slf4j
public class LoggingAttributeFinderModule extends BaseAttributeFinderModule {

	@Override
	public boolean isDesignatorSupported() {
		return true;
	}

	@Override
	public EvaluationResult findAttribute(URI attributeType, URI attributeId,
			String issuer, URI category, EvaluationCtx context)
	{
		log.info("Arguments: attr-type: {}, attr-id: {}, issuer: {}, category: {}, context: {}",
				attributeType, attributeId, issuer, category, context);
		return createEmptyResult(attributeType);
	}
}