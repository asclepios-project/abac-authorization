/*
 * Copyright (C) 2017-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.asclepios.authorization.abac.server.pdp.finder;

import inet.ipaddr.IPAddress;
import inet.ipaddr.IPAddressString;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.wso2.balana.XACMLConstants;
import org.wso2.balana.cond.EvaluationResult;
import org.wso2.balana.ctx.EvaluationCtx;
import org.wso2.balana.utils.Constants.PolicyConstants;

import java.net.URI;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * IP Address Attribute Finder Module
 */
@Slf4j
public class IpAddressAttributeFinderModule extends BaseAttributeFinderModule {

	public final static URI IP_ADDRESS_ATTRIBUTE_URI = URI.create("http-req-remote-address");
	public final static Set<String> ATTRIBUTE_SET = Collections.unmodifiableSet(
			new HashSet<String>() {{
				add("ip-address-type");
				add("ip-address-version");
			}}
	);

	@Override
	public boolean isDesignatorSupported() {
		return true;
	}

	@Override
	public EvaluationResult findAttribute(URI attributeType, URI attributeId,
										  String issuer, URI category, EvaluationCtx context)
	{
		log.debug("Arguments: attr-type: {}, attr-id: {}, issuer: {}, category: {}, context: {}",
				attributeType, attributeId, issuer, category, context);

		String attrId = attributeId.toASCIIString();
		if (!ATTRIBUTE_SET.contains(attrId)) {
			log.debug("--------    Attribute not supported: {}", attrId);
			return createEmptyResult(attributeType);
		}

		if (attributeMatches(
				PolicyConstants.STRING_DATA_TYPE, attributeType,
				attrId, attributeId,
				XACMLConstants.ENT_CATEGORY, category))
		{
			// Get remote IP address from HTTP request (if included in XACML request)
			log.debug("--------    Searching for request IP address...");
			String ipAddr = findStringAttribute(attributeType, IP_ADDRESS_ATTRIBUTE_URI, issuer, category, context);
			log.debug("--------    Found IP address: {}", ipAddr);

			// Check IP address
			if (StringUtils.isNotBlank(ipAddr)) {
				String value = null;
				IPAddress ipAddress = new IPAddressString(ipAddr).getAddress();
				if ("ip-address-type".equalsIgnoreCase(attrId)) {
					if (ipAddress.isLoopback())
						value = "loopback";
					else if (ipAddress.isLinkLocal())
						value = "link-local";
					else if (ipAddress.isLocal())
						value = "local";
					else
						value = "remote";
				} else
				if ("ip-address-version".equalsIgnoreCase(attrId)) {
					value = ipAddress.getIPVersion().toString();
				}

				log.debug("--------    Attribute value: {} = {}", attrId, value);
				return createStringResult(category, attributeType, attributeId, value, context);
			} else {
				return createEmptyStringResult();
			}
		}

		return createEmptyResult(attributeType);
	}
}
