/*
 * Copyright (C) 2017-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.asclepios.authorization.abac.server.log;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;

/**
 * LogService
 */
@Slf4j
@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class LogJob {
    private final String requestRef;
    private final JOB_TYPE type;
    private final Serializable objectToLog;
    private String description;

    enum JOB_TYPE { REQUEST, RESULT, ATTRIBUTE }
}
