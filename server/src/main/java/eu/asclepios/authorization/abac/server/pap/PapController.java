/*
 * Copyright (C) 2017-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.asclepios.authorization.abac.server.pap;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.TEXT_PLAIN_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.*;

@Slf4j
@Validated
@RestController
@RequiredArgsConstructor
@RequestMapping("/pap")
public class PapController {

	private final PapProperties properties;
	private final FileService fileService;

	// GET /
	// Description: It's here just for testing if server is up
	@RequestMapping(value = "", method = GET, produces = TEXT_PLAIN_VALUE)
	public String index() { return "PAP simulator server is running"; }

	// GET /pap/abac-policy
	// Description: List all ABAC policies in PAP
	@RequestMapping(value = "/abac-policy", method = GET, produces = APPLICATION_JSON_VALUE)
	public List<String> listAbacPolicies() throws IOException {
		return fileService.listPolicies(properties.getAbacPoliciesDirectory(), properties.getAbacPolicyIdPattern());
	}

	// GET /pap/abac-policy
	// Description: Get an ABAC policy from PAP
	@RequestMapping(value = "/abac-policy/{policy_id}", method = GET, produces = TEXT_PLAIN_VALUE)
	public String getAbacPolicy(@PathVariable("policy_id") String policyId) throws IOException {
		return fileService.readPolicy(policyId, "getAbacPolicy", "ABAC Policy",
				properties.getAbacPolicyFileNamePattern(), properties.getAbacPoliciesDirectory());
	}

	// PUT /pap/abac-policy
	// Description: Upload an ABAC policy (in XACML) to PAP
	@RequestMapping(value = "/abac-policy/{policy_id}", method = PUT, produces = TEXT_PLAIN_VALUE)
	public String addAbacPolicy(@PathVariable("policy_id") String policyId, @RequestBody String policyContent) throws IOException {
		fileService.storePolicy(policyId, policyContent, "addAbacPolicy", "ABAC Policy",
				properties.getAbacPolicyFileNamePattern(), properties.getAbacPoliciesDirectory());

		return "ABAC POLICY UPLOADED";
	}

	// DELETE /pap/abac-policy
	// Description: Deletes an ABAC policy from PAP
	@RequestMapping(value = "/abac-policy/{policy_id}", method = DELETE, produces = TEXT_PLAIN_VALUE)
	public String deleteAbacPolicy(@PathVariable("policy_id") String policyId) throws IOException {
		boolean deleted = fileService.deletePolicy(policyId, "deleteAbacPolicy", "ABAC Policy",
				properties.getAbacPolicyFileNamePattern(), properties.getAbacPoliciesDirectory());
		return "ABAC POLICY DELETED: "+deleted;
	}

	// ------------------------------------------------------------------------

	// GET /pap/abe-policy
	// Description: List all ABE policies in PAP
	@RequestMapping(value = "/abe-policy", method = GET, produces = APPLICATION_JSON_VALUE)
	public List<String> listAbePolicies() throws IOException {
		return fileService.listPolicies(properties.getAbePoliciesDirectory(), properties.getAbePolicyIdPattern());
	}

	// GET /pap/abe-policy
	// Description: Get an ABE policy from PAP
	@RequestMapping(value = "/abe-policy/{policy_id}", method = GET, produces = TEXT_PLAIN_VALUE)
	public String getAbePolicy(@PathVariable("policy_id") String policyId) throws IOException {
		return fileService.readPolicy(policyId, "getAbePolicy", "ABE Policy",
				properties.getAbePolicyFileNamePattern(), properties.getAbePoliciesDirectory());
	}

	// PUT /pap/abe-policy
	// Description: Upload an ABE policy (in Text) to PAP
	@RequestMapping(value = "/abe-policy/{policy_id}", method = PUT, produces = TEXT_PLAIN_VALUE)
	public String addAbePolicy(@PathVariable("policy_id") String policyId, @RequestBody String policyContent) throws IOException {
		fileService.storePolicy(policyId, policyContent, "addAbePolicy", "ABE Policy",
				properties.getAbePolicyFileNamePattern(), properties.getAbePoliciesDirectory());

		return "ABE POLICY UPLOADED";
	}

	// DELETE /pap/abe-policy
	// Description: Deletes an ABE policy from PAP
	@RequestMapping(value = "/abe-policy/{policy_id}", method = DELETE, produces = TEXT_PLAIN_VALUE)
	public String deleteAbePolicy(@PathVariable("policy_id") String policyId) throws IOException {
		boolean deleted = fileService.deletePolicy(policyId, "deleteAbePolicy", "ABE Policy",
				properties.getAbePolicyFileNamePattern(), properties.getAbePoliciesDirectory());
		return "ABAC POLICY DELETED: "+deleted;
	}
}
