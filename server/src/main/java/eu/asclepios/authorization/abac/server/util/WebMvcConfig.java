/*
 * Copyright (C) 2017-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.asclepios.authorization.abac.server.util;

import eu.asclepios.authorization.abac.server.pap.PapProperties;
import eu.asclepios.authorization.abac.server.properties.AuthorizationServiceProperties;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
@Configuration
@AllArgsConstructor
public class WebMvcConfig implements WebMvcConfigurer {
    private AuthorizationServiceProperties pdpProperties;
    private PapProperties papProperties;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        if (StringUtils.isNoneBlank(pdpProperties.getPdp().getAccessKey())) {
            log.info("Using API Key authentication in PDP");
            registry.addInterceptor(new ApiKeyCheckerInterceptor("PDP", pdpProperties.getPdp().getAccessKey().trim(), "/checkJsonAccessRequest"));
        }
        if (StringUtils.isNoneBlank(papProperties.getApiKey())) {
            log.info("Using API Key authentication in PAP");
            registry.addInterceptor(new ApiKeyCheckerInterceptor("PAP", papProperties.getApiKey().trim(), "/pap"));
        }
    }

    @AllArgsConstructor
    public static class ApiKeyCheckerInterceptor implements HandlerInterceptor {
        private String id;
        private String apiKey;
        private String contextPrefix;

        public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
            // protected path starts with 'contextPrefix' value
            String path = request.getRequestURI();
            if (! StringUtils.startsWith(path, contextPrefix))
                return true;

            // if no API Key specified in configuration then accept request
            if (StringUtils.isBlank(apiKey))
                return true;

            // check if an API Key is specified in request headers
            String headerApiKey = request.getHeader("X-API-KEY");
            if (StringUtils.isNotBlank(headerApiKey))
                if (apiKey.equals(headerApiKey.trim())) return true;

            // check if an API Key is specified in request's query string
            String qsApiKey = request.getParameter("api-key");
            if (StringUtils.isNotBlank(qsApiKey))
                if (apiKey.equals(qsApiKey.trim())) return true;

            // else reject request
            throw new ApiKeyException("Invalid or no API Key provided: id="+id);
        }

        public static class ApiKeyException extends Exception {
            public ApiKeyException(String message) { super(message); }
        }
    }
}
