/*
 * Copyright (C) 2017-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.asclepios.authorization.abac.server.log;

import eu.asclepios.authorization.abac.server.log.entity.Attribute;
import eu.asclepios.authorization.abac.util.CheckAccessRequest;
import eu.asclepios.authorization.abac.util.PdpResult;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Service;

/**
 * LogWorkerService
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class LogWorkerService implements Runnable, ApplicationListener<ApplicationReadyEvent> {
    private final LogService logService;
    private final TaskExecutor taskExecutor;
    private final LogDatabaseService databaseService;

    @Override
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {
        taskExecutor.execute(this);
        log.debug("Log worker started");
    }

    @Override
    public void run() {
        while (true) {
            try {
                // Pick/Wait a LogJob from the queue
                LogJob job;
                while ((job = logService.getJob())==null) Thread.sleep(100);

                // Process LogJob
                log.debug("Log worker: Processing job: {}", job);
                String requestRef = job.getRequestRef();
                if (job.getType()== LogJob.JOB_TYPE.REQUEST) {
                    databaseService.logRequest(requestRef, (CheckAccessRequest)job.getObjectToLog());
                } else
                if (job.getType()== LogJob.JOB_TYPE.RESULT) {
                    databaseService.logResult(requestRef, (PdpResult)job.getObjectToLog());
                } else
                if (job.getType()== LogJob.JOB_TYPE.ATTRIBUTE) {
                    databaseService.logAttribute(requestRef, (Attribute)job.getObjectToLog());
                }
                log.debug("Log worker: Processing job completed: {}", job);

            } catch (Throwable t) {
                log.error("Exception occurred in log worker: ", t);
            }
        }
    }
}
