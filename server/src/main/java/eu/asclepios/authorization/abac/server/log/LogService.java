/*
 * Copyright (C) 2017-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.asclepios.authorization.abac.server.log;

import eu.asclepios.authorization.abac.server.log.entity.Attribute;
import eu.asclepios.authorization.abac.util.CheckAccessRequest;
import eu.asclepios.authorization.abac.util.PdpResult;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.net.URI;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * LogService
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class LogService {
    private final ConcurrentLinkedQueue<LogJob> jobQueue = new ConcurrentLinkedQueue<LogJob>();

    private void addJob(String requestRef, LogJob.JOB_TYPE type, Serializable object) {
        jobQueue.add(new LogJob(requestRef, type, object));
    }

    private void addJob(String requestRef, LogJob.JOB_TYPE type, Serializable object, String description) {
        jobQueue.add(new LogJob(requestRef, type, object, description));
    }

    LogJob getJob() {
        return jobQueue.poll();
    }

    public void logRequest(CheckAccessRequest request, String requestRef) {
        addJob(requestRef, LogJob.JOB_TYPE.REQUEST, request, "Request");
    }

    public void logResult(PdpResult result, String requestRef) {
        addJob(requestRef, LogJob.JOB_TYPE.RESULT, result, "Decision");
    }

    public void logAttribute(URI category, URI type, URI attributeId, String value, String issuer, String requestRef) {
        Attribute attribute = new Attribute(
                -1,     // will be completed later
                category.toASCIIString(),
                attributeId.toASCIIString(),
                value,
                type.toASCIIString(),
                issuer);

        addJob(requestRef, LogJob.JOB_TYPE.ATTRIBUTE, attribute, "Attribute");
    }
}
