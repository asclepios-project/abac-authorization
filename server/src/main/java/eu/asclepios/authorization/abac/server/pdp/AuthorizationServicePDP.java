/*
 * Copyright (C) 2017-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.asclepios.authorization.abac.server.pdp;

import eu.asclepios.authorization.abac.server.log.LogService;
import eu.asclepios.authorization.abac.server.pdp.finder.DBAttributeFinderModule;
import eu.asclepios.authorization.abac.server.pdp.finder.InMemoryAttributeFinderModule;
import eu.asclepios.authorization.abac.server.properties.AuthorizationServiceProperties;
import eu.asclepios.authorization.abac.server.util.AttributeUtil;
import eu.asclepios.authorization.abac.util.CheckAccessRequest;
import eu.asclepios.authorization.abac.util.PdpResult;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;
import org.wso2.balana.*;
import org.wso2.balana.attr.*;
import org.wso2.balana.ctx.*;
import org.wso2.balana.ctx.xacml3.RequestCtx;
import org.wso2.balana.finder.AttributeFinder;
import org.wso2.balana.finder.AttributeFinderModule;
import org.wso2.balana.finder.PolicyFinder;
import org.wso2.balana.finder.impl.FileBasedPolicyFinderModule;
import org.wso2.balana.xacml3.Attributes;
import org.wso2.balana.xacml3.Obligation;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.*;
import java.net.URI;
import java.text.ParseException;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

/**
 * AuthorizationServicePDP
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class AuthorizationServicePDP implements ApplicationContextAware {
    private final AuthorizationServiceProperties properties;
    private final LogService logService;
    private PDP pdp;
    private DirectoryWatcher directoryWatcher;

    private PolicyFinder policyFinder;
    private FileBasedPolicyFinderModule policyFinderModule;
    private Set<String> policyFileSet;

    private static final AtomicLong requestCounter = new AtomicLong(0);

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        initPolicyDirectoryWatcher();
        try {
            sun.misc.Signal.handle(new sun.misc.Signal("HUP"), signal -> {
                log.warn("RECEIVED SIGNAL: {}", signal);
                reloadPolicies();
            });
        } catch (Exception e) {
            log.error("Unable to set SIGHUP handler: ", e);
            log.warn("*** YOU MUST RELOAD POLICIES MANUALLY ***");
        }
    }

    protected void initPolicyDirectoryWatcher() {
        try {
            log.debug("Starting policy directory watcher: policy-dir={}, policy-file-extension={}, polling-interval={}",
                    properties.getPdp().getPoliciesDir(), properties.getPdp().getPoliciesExtension(), properties.getPdp().getRestartPollingInterval());
            (directoryWatcher = new DirectoryWatcher()).startDirectoryWatcher(
                    properties.getPdp().getPoliciesDir(),
                    properties.getPdp().getPoliciesExtension(),
                    properties.getPdp().getRestartPollingInterval(),
                    this::reloadPolicies);
            log.debug("Policy directory watcher started");
        } catch (IOException e) {
            throw new BeanInitializationException("Failed to initialize Policy directory watcher", e);
        }
    }

    public void reloadPolicies() {
        try {
            String policyDirectoryLocation = properties.getPdp().getPoliciesDir();
            String policyFilesExtension = properties.getPdp().getPoliciesExtension();
            log.info("Getting policy files from: {}", policyDirectoryLocation);
            Set<String> fileNames = getFilesInFolder(policyDirectoryLocation, policyFilesExtension);
            log.info("Policy files found: {}", fileNames.size());
            log.debug("Policy files found: {}", fileNames);

            // Reloading policies...
            log.info("Reloading policies...");
            policyFileSet.clear();
            policyFileSet.addAll(fileNames);
            policyFinderModule.init(policyFinder);
            log.info("Reloading policies... done");
        } catch (Exception e) {
            log.error("Reloading policies... failed: ", e);
        }
    }

    @PostConstruct
    public void initPdp() throws Exception {
        try {
            _initPdp();
        } catch (Exception ex) {
            log.error("Exception thrown during PDP initialization: ", ex);
            throw ex;
        }
    }

    private synchronized void _initPdp() throws Exception {
        log.info("Initializing PDP...");

        // Get PDP configuration
        String configFileLocation = properties.getPdp().getBalanaConfigFile();
        String policyDirectoryLocation = properties.getPdp().getPoliciesDir();
        String policyFilesExtension = properties.getPdp().getPoliciesExtension();
        System.setProperty(ConfigurationStore.PDP_CONFIG_PROPERTY, configFileLocation);
        System.setProperty(FileBasedPolicyFinderModule.POLICY_DIR_PROPERTY, policyDirectoryLocation);

        // Configure policies directory (PAP)
        log.info("Getting policy files from: {}", policyDirectoryLocation);
        Set<String> fileNames = getFilesInFolder(policyDirectoryLocation, policyFilesExtension);
        log.info("Policy files found: {}", fileNames.size());
        log.debug("Policy files found: {}", fileNames);
        PolicyFinder pf = new PolicyFinder();
        FileBasedPolicyFinderModule pfm = new FileBasedPolicyFinderModule(fileNames);
        pf.setModules(Collections.singleton(pfm));
        pfm.init(pf);
        this.policyFinder = pf;
        this.policyFinderModule = pfm;
        this.policyFileSet = fileNames;

        // Configure attribute finders (PIP's)
        PDPConfig pdpConfig = Balana.getInstance().getPdpConfig();
        AttributeFinder attributeFinder = pdpConfig.getAttributeFinder();
        java.util.List<AttributeFinderModule> finderModules = attributeFinder.getModules();

//XXX: EXPERIMENTAL: adding In-Memory attribute finder
        // Configuring In-memory attribute finder
        finderModules.add(new InMemoryAttributeFinderModule(null));

        // Configuring Attribute finders by class
        log.info("Configured attribute finder module classes: {}", properties.getPdp().getAttributeFinderClasses());
        if (properties.getPdp().getAttributeFinderClasses()!=null) {
            for (String classStr : properties.getPdp().getAttributeFinderClasses()) {
                if (StringUtils.isNotBlank(classStr)) {
                    Class<?> clazz = Class.forName(classStr.trim());
                    AttributeFinderModule newFinder = (AttributeFinderModule) clazz.newInstance();
                    log.info("  Instantiating attribute finder module: {}", newFinder);
                    finderModules.add(newFinder);
                }
            }
        }

        // Configuring Database attribute finder(s)
        int cntPip = -1;
        if (properties.getPip()!=null) {
            for (AuthorizationServiceProperties.PipConfig pipCfg : properties.getPip()) {
                log.info("Configuring PIP #{}: {}", cntPip++, pipCfg);

                // Initialize attribute finder module from PIP configuration
                AttributeFinderModule afm = null;
                if (pipCfg.getDb() != null) {
                    afm = new DBAttributeFinderModule(pipCfg.getDb(), "" + cntPip);
                }

                // Add new attribute finder module in PDP configuration
                if (afm != null) {
                    finderModules.add(afm);
                } else {
                    log.info("PIP configuration is not valid. Ignoring it: {}", pipCfg);
                }
            }
        }
        attributeFinder.setModules(finderModules);

        // Initialize PDP
        pdp = new PDP(new PDPConfig(attributeFinder, pf, null, true));

        log.info("PDP initialized");
    }

    @PreDestroy
    public void cleanup() {
    }

    /*public PDP_RESULT evaluateRequest(String xacmlRequestStr) {
        try {
            return __evaluateRequest(xacmlRequestStr);
        } catch (Exception ex) {
            log.error("EXCEPTION while evaluating access request: ", ex);
            return PDP_RESULT.ERROR;
        }
    }

    protected PDP_RESULT __evaluateRequest(String xacmlRequest) throws IOException {
        // Evaluate access request against policies
        log.debug("XACML Request:\n{}\n", xacmlRequest);
        String xacmlResponse = pdp.evaluate(xacmlRequest);
        log.debug("XACML Response:\n{}\n", xacmlResponse);

        // Fulfill tracking obligations
        fulfillTrackingObligations(xacmlResponse);

        // Extract XACML response decision
        PDP_RESULT result;
        if (xacmlResponse.indexOf("<Decision>Permit</Decision>") > 0) result = PDP_RESULT.PERMIT;
        else if (xacmlResponse.indexOf("<Decision>Deny</Decision>") > 0) result = PDP_RESULT.DENY;
        else if (xacmlResponse.indexOf("<Decision>Indeterminate</Decision>") > 0) result = PDP_RESULT.INDETERMINATE;
        else if (xacmlResponse.indexOf("<Decision>NotApplicable</Decision>") > 0) result = PDP_RESULT.NOT_APPLICABLE;
        else result = PDP_RESULT.ERROR;

        return result;
    }*/

    public PdpResult evaluateRequest(CheckAccessRequest request) {
        try {
            return __evaluateRequest(request);
        } catch (Exception ex) {
            log.error("EXCEPTION while evaluating access request: ", ex);
            return new PdpResult(PDP_RESULT.ERROR.toString(), null, null, ex.getMessage(), null);
        }
    }

    protected PdpResult __evaluateRequest(CheckAccessRequest request) throws ParsingException, ParseException {
        log.debug("XACML Request:\n{}\n", request);

        // Create request context for the given 'check access request'
        String requestId = getUniqueRequestId();
        logService.logRequest(request, requestId);
        RequestCtx xacmlRequestCtx = createXacmlRequest(request.getXacmlRequest(), requestId);

        // Evaluate access request against policies
        ResponseCtx responseCtx = pdp.evaluate(xacmlRequestCtx);
        String xacmlResponse = responseCtx.encode();
        log.debug("XACML Response:\n{}\n", xacmlResponse);

        // Check results count
        if (responseCtx.getResults().size()==0) {
            throw new RuntimeException("PDP request evaluation returned No results");
        } else
        if (responseCtx.getResults().size()>1) {
            throw new RuntimeException("PDP request evaluation returned Many results: "+responseCtx.getResults().size());
        }

        // Process PDP response
        PdpResult pdpResult;
        Iterator<AbstractResult> it = responseCtx.getResults().iterator();
        AbstractResult result = it.next();
        pdpResult = encodeResult(result);

        // Check for errors
        log.debug("Evaluation status: {}", result.getStatus().getCode());
        log.info(">>> RESULT: request-id={}, decision={}, pdp-result={}", requestId, pdpResult.getDecision(), pdpResult);
        logService.logResult(pdpResult, requestId);

        // Fulfill obligations - Not needed since PDP result is logged (see above)
        //fulfillObligations(requestId, result.getObligations());

        return pdpResult;
    }

    protected PdpResult encodeResult(AbstractResult result) {
        // get result info
        int decision = result.getDecision();
        String statusCodes = result.getStatus().getCode().stream()
                .map(s -> {
                    PDP_RESULT_STATUS resultStatus;
                    if (Status.STATUS_OK.equals(s))
                        resultStatus = PDP_RESULT_STATUS.OK;
                    else if (Status.STATUS_SYNTAX_ERROR.equals(s))
                        resultStatus = PDP_RESULT_STATUS.SYNTAX_ERROR;
                    else if (Status.STATUS_PROCESSING_ERROR.equals(s))
                        resultStatus = PDP_RESULT_STATUS.PROCESSING_ERROR;
                    else if (Status.STATUS_MISSING_ATTRIBUTE.equals(s))
                        resultStatus = PDP_RESULT_STATUS.MISSING_ATTRIBUTE_ERROR;
                    else
                        resultStatus = PDP_RESULT_STATUS.OTHER_ERROR;
                    return resultStatus.toString();
                })
                .collect(Collectors.joining());
        String statusDetails = result.getStatus().getDetail()!=null ? result.getStatus().getDetail().getEncoded() : null;
        String statusMessage = result.getStatus().getMessage();

        // get result obligations
        Map<String,Map<String,String>> obligationsMap = new HashMap<>();
        for (ObligationResult obligation : result.getObligations()) {
            String obligationId = ((Obligation)obligation).getObligationId().toASCIIString();
            Map<String,String> assignmentMap = new HashMap<>();
            obligationsMap.put(obligationId, assignmentMap);
            for (AttributeAssignment assignment : ((Obligation) obligation).getAssignments()) {
                String assignmentId = assignment.getAttributeId().toASCIIString();
                String assignmentContent = assignment.getContent();
                assignmentMap.put(assignmentId, assignmentContent);
            }
        }

        // get response decision
        PDP_RESULT pdpDecision;
        switch (decision) {
            case AbstractResult.DECISION_PERMIT:
                pdpDecision = PDP_RESULT.PERMIT; break;
            case AbstractResult.DECISION_DENY:
                pdpDecision = PDP_RESULT.DENY;  break;
            case AbstractResult.DECISION_NOT_APPLICABLE:
                pdpDecision = PDP_RESULT.NOT_APPLICABLE;  break;
            default: pdpDecision = PDP_RESULT.INDETERMINATE;
        }

        return new PdpResult(pdpDecision.toString(), statusCodes, statusDetails, statusMessage, obligationsMap);
    }

    protected String getUniqueRequestId() {
        return requestCounter.get()+"-"+System.nanoTime()+"-"+((long)(1000000L*Math.random()));
    }

    public String createXacmlRequest(CheckAccessRequest request) throws ParsingException, ParseException {
        return createXacmlRequest(request, false);
    }

    public String createXacmlRequest(CheckAccessRequest request, boolean addUniqueId) throws ParsingException, ParseException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        createXacmlRequest(request.getXacmlRequest(), addUniqueId).encode(baos);
        return baos.toString();
    }

    public RequestCtx createXacmlRequest(Map<String,Object> requestProperties, boolean addUniqueId) throws ParsingException, ParseException {
        return createXacmlRequest(requestProperties, addUniqueId ? getUniqueRequestId() : null);
    }

    protected RequestCtx createXacmlRequest(Map<String,Object> requestProperties, String requestId) throws ParsingException, ParseException {
        // Create request context from provided request properties
        RequestCtx xacmlRequestCtx = createXacmlRequest(requestId, requestProperties);

        // Add a unique request context id
        /*if (StringUtils.isNotBlank(uniqueId)) {
            Attribute reqCtxId = new Attribute(
                    URI.create("request-context-id"), null,
                    new DateTimeAttribute(),
                    new StringAttribute(uniqueId), -1);
            Attributes envAttrs = xacmlRequestCtx.getAttributesSet().stream()
                    .filter(attribs -> XACMLConstants.ENT_CATEGORY.equals(attribs.getCategory().toASCIIString()))
                    .findFirst()
                    .orElse(null);
            if (envAttrs==null) {
                envAttrs = new Attributes(URI.create(XACMLConstants.ENT_CATEGORY), Collections.emptySet());
                xacmlRequestCtx.getAttributesSet().add(envAttrs);
            }
            envAttrs.getAttributes().add(reqCtxId);
        }*/

        return xacmlRequestCtx;
    }

    public RequestCtx createXacmlRequest(String requestId, Map<String,Object> requestProperties) throws ParsingException, ParseException {
        // categorize attributes by 'attribute category'
        Map<String, List<Map.Entry<String,Object>>> categoryAttrs = new HashMap<>();
        for (Map.Entry<String,Object> entry : requestProperties.entrySet()) {
            // get attribute key and value
            String key = entry.getKey();
            Object value = entry.getValue();

            // get attribute category (if unknown it defaults to 'environment')
            String category = AttributeUtil.getInstance().getAttributeCategory(key, XACMLConstants.ENT_CATEGORY);

            // store attribute entry in categoryAttrs
            List attrs = categoryAttrs.get(category);
            if (attrs==null) { attrs = new Vector(); categoryAttrs.put(category, attrs); }
            attrs.add( entry );
        }

        // build XACML request context
        PDPRequestCtx reqCtx = new PDPRequestCtx(requestId, null, new HashSet<>(), true, false, null, null);
        reqCtx.setLogService(logService);
        for (String category : categoryAttrs.keySet())
        {
            // open attributes category
            Attributes attributes = new Attributes(URI.create(category), new HashSet<>());
            reqCtx.getAttributesSet().add(attributes);

            for (Map.Entry<String,Object> entry : categoryAttrs.get(category)) {
                // get attribute key and value
                String key = entry.getKey();
                Object value = entry.getValue();

                // check if attribute is a Collection
                log.debug("createXacmlRequest: attribute-key={}, value-class={}", key, value!=null ? value.getClass().getName() : null);
                if (value!=null && Collection.class.isAssignableFrom(value.getClass())) {
                    log.warn("createXacmlRequest: Bag will be used for attribute: {}", key);
/*XXX: BUG!!!
Throws: java.lang.UnsupportedOperationException: Bags cannot be encoded
Reason:
See: https://github.com/wso2/balana/blob/master/modules/balana-core/src/main/java/org/wso2/balana/attr/BagAttribute.java
    / **
     * Because a bag cannot be included in a request/response or a policy, this will always throw an
     * <code>UnsupportedOperationException</code>.
     * /
    public String encode() {
        throw new UnsupportedOperationException("Bags cannot be encoded");
    }
*/
                    Collection col = (Collection)value;
                    java.util.Iterator it = col.iterator();

                    if (!it.hasNext()) continue;

                    Object firstValue = it.next();
                    String firstValueType = getXsdType(firstValue);
                    if (firstValueType==null) firstValueType = "http://www.w3.org/2001/XMLSchema#string";

                    // append attribute to XACML request
                    List<AttributeValue> bagValues = new ArrayList<>();
                    bagValues.add(createAttributeValue(firstValueType, value.toString()));
                    Attribute attr = new Attribute(
                            URI.create(key),
                            null,
                            new DateTimeAttribute(),
                            new BagAttribute(URI.create(firstValueType), bagValues),
                            true, 3);
                    attributes.getAttributes().add(attr);

                    while (it.hasNext()) {
                        Object bagItemValue = it.next();

                        String type = getXsdType(bagItemValue);
                        if (type==null) type = AttributeUtil.getInstance().getAttributeType(key, "http://www.w3.org/2001/XMLSchema#string");
                        log.warn("createXacmlRequest: Bag element value type: {}", type);

                        String strValue = bagItemValue!=null ? bagItemValue.toString() : "";
                        bagValues.add(createAttributeValue(type, strValue));
                    }
                } else
                // get XSD attribute type (if unknown it defaults to 'xsd:string')
                {
                    log.debug("createXacmlRequest: XSD single value will be used for attribute: {}", key);
                    String type = getXsdType(value);
                    if (type==null) type = AttributeUtil.getInstance().getAttributeType(key, "http://www.w3.org/2001/XMLSchema#string");
                    log.debug("createXacmlRequest: XSD value type: {}", type);

                    // append attribute to XACML request
                    AttributeValue attrVal = createAttributeValue(type, value!=null ? value.toString() : "");
                    Attribute newAttr = new Attribute(URI.create(key), null, new DateTimeAttribute(), attrVal, true, 3);
                    attributes.getAttributes().add(newAttr);
                }
            }

            // close attributes category
        }

        return reqCtx;
    }

    private AttributeValue createAttributeValue(String type, String value) throws ParsingException, ParseException {
        switch (type) {
            case "http://www.w3.org/2001/XMLSchema#boolean":
                return BooleanAttribute.getInstance(value);
            case "http://www.w3.org/2001/XMLSchema#integer":
                return IntegerAttribute.getInstance(value);
            case "http://www.w3.org/2001/XMLSchema#double":
                return DoubleAttribute.getInstance(value);
            case "http://www.w3.org/2001/XMLSchema#dateTime":
                return DateTimeAttribute.getInstance(value);
            default:
                return new StringAttribute(value);
        }
        //throw new IllegalArgumentException("Unknown or Not supported XACML type: "+type);
    }

    protected String getXsdType(Object value) {
        if (value==null) return null;
        String typeStr = value.getClass().getName();
        if (typeStr.equals("boolean")) return "http://www.w3.org/2001/XMLSchema#boolean";
        else if (typeStr.equals("byte")) return "http://www.w3.org/2001/XMLSchema#integer";
        else if (typeStr.equals("short")) return "http://www.w3.org/2001/XMLSchema#integer";
        else if (typeStr.equals("int")) return "http://www.w3.org/2001/XMLSchema#integer";
        else if (typeStr.equals("long")) return "http://www.w3.org/2001/XMLSchema#integer";
        else if (typeStr.equals("float")) return "http://www.w3.org/2001/XMLSchema#double";
        else if (typeStr.equals("double")) return "http://www.w3.org/2001/XMLSchema#double";
        else if (typeStr.equals("java.lang.Boolean")) return "http://www.w3.org/2001/XMLSchema#boolean";
        else if (typeStr.equals("java.lang.Byte")) return "http://www.w3.org/2001/XMLSchema#integer";
        else if (typeStr.equals("java.lang.Short")) return "http://www.w3.org/2001/XMLSchema#integer";
        else if (typeStr.equals("java.lang.Integer")) return "http://www.w3.org/2001/XMLSchema#integer";
        else if (typeStr.equals("java.lang.Long")) return "http://www.w3.org/2001/XMLSchema#integer";
        else if (typeStr.equals("java.lang.Float")) return "http://www.w3.org/2001/XMLSchema#double";
        else if (typeStr.equals("java.lang.Double")) return "http://www.w3.org/2001/XMLSchema#double";
        else if (typeStr.equals("java.lang.Date")) return "http://www.w3.org/2001/XMLSchema#dateTime";
        else return null;
    }

    /*private void fulfillTrackingObligations(String xacmlResponse) {
        if (StringUtils.isBlank(xacmlResponse)) return;

        int startPos = xacmlResponse.indexOf("<Obligations>");
        int endPos = xacmlResponse.indexOf("</Obligations>");
        if (0<startPos && startPos<endPos) {
            String obligationsStr = xacmlResponse.substring(startPos+13, endPos);
            if (StringUtils.isBlank(obligationsStr)) return;

            String[] obligations = obligationsStr.split("<Obligation");
            for (String s : obligations) {
                startPos = s.indexOf("ObligationId=\"");
                endPos = s.indexOf("\"", startPos+14);
                if (0<=startPos && (startPos+14)<endPos) {
                    String obligationId = s.substring(startPos+14, endPos).trim();
                    log.info("Tracking Obligation: {}", obligationId);
                }
            }
        }
    }*/

    private void fulfillObligations(String requestId, List<ObligationResult> obligations) {
        for (ObligationResult obligation : obligations) {
            URI obligationId = ((Obligation)obligation).getObligationId();
            log.debug("OBLIGATION: request-id={}, obligation-id={}", requestId, obligationId);
            if (log.isDebugEnabled()) log.debug("OBLIGATION: {}", obligation.encode());

            Map<String,String> assignmentMap = new HashMap<>();
            if (((Obligation)obligation).getAssignments()!=null) {
                List<AttributeAssignment> assignments = ((Obligation) obligation).getAssignments();
                log.debug("OBLIGATION: {} assignments", assignments.size());
                for (AttributeAssignment assignment : assignments) {
                    if (log.isDebugEnabled()) log.debug("OBLIGATION ASSIGNMENT: {}", assignment.encode());
                    String attrId = assignment.getAttributeId().toASCIIString();
                    String attrValue = assignment.getContent();
                    log.debug("OBLIGATION ASSIGNMENT: request-id={}, attribute-id={}, attribute-value={}", requestId, attrId, attrValue);
                    assignmentMap.put(attrId, attrValue);
                }
            } else {
                log.debug("OBLIGATION: No assignments");
            }

            log.info("OBLIGATION: request-id={}, obligation-id={}, assignments={}", requestId, obligationId, assignmentMap);
        }
    }

    private static Set<String> getFilesInFolder(String directory, final String extension) throws FileNotFoundException {
        File dir = new File(directory);
        String[] children = null;
        if (StringUtils.isNotEmpty(extension)) {
            FilenameFilter filter = (f, name) -> name.endsWith(StringUtils.defaultIfEmpty(extension,""));
            children = dir.list(filter);
        } else {
            children = dir.list();
        }
        if (children == null) {
        	log.error("Policies directory not found: {}", directory);
        	throw new FileNotFoundException("Policies directory not found: "+directory);
		}
        HashSet<String> result = new HashSet<>();
        String fileSep = System.getProperty("file.separator");
        for (int i = 0; i < children.length; i++) {
            result.add(directory + fileSep + children[i]);
        }
        return result;
    }

    public enum PDP_RESULT {
        PERMIT, DENY, INDETERMINATE, NOT_APPLICABLE, ERROR
    }

    public enum PDP_RESULT_STATUS {
        OK, SYNTAX_ERROR, PROCESSING_ERROR, MISSING_ATTRIBUTE_ERROR, OTHER_ERROR
    }
}
