/*
 * Copyright (C) 2017-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.asclepios.authorization.abac.server.pdp;

import eu.asclepios.authorization.abac.server.log.LogService;
import lombok.Getter;
import lombok.Setter;
import org.w3c.dom.Node;
import org.wso2.balana.ctx.xacml3.RequestCtx;
import org.wso2.balana.xacml3.Attributes;
import org.wso2.balana.xacml3.MultiRequests;
import org.wso2.balana.xacml3.RequestDefaults;

import javax.validation.constraints.NotNull;
import java.util.Set;

@Getter
@Setter
public class PDPRequestCtx extends RequestCtx {
    private final String requestId;
    private LogService logService;

    public PDPRequestCtx(@NotNull String requestId, Set<Attributes> attributesSet, Node documentRoot) {
        super(attributesSet, documentRoot);
        this.requestId = requestId;
    }

    public PDPRequestCtx(@NotNull String requestId, Node documentRoot, Set<Attributes> attributesSet, boolean returnPolicyIdList, boolean combinedDecision, MultiRequests multiRequests, RequestDefaults defaults) throws IllegalArgumentException {
        super(documentRoot, attributesSet, returnPolicyIdList, combinedDecision, multiRequests, defaults);
        this.requestId = requestId;
    }
}
