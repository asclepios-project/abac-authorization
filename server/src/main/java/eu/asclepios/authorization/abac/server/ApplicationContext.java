/*
 * Copyright (C) 2017-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.asclepios.authorization.abac.server;

import eu.asclepios.authorization.abac.server.properties.AuthorizationServiceProperties;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;

@Configuration
@AllArgsConstructor(onConstructor = @__({@Autowired}))
public class ApplicationContext {
	//private final AuthorizationServicePDP pdp;
	//private final DatabaseContextBean contextDb;
	private final AuthorizationServiceProperties properties;

	@Bean
	public TaskExecutor taskExecutor() { return new SimpleAsyncTaskExecutor(); }
}
