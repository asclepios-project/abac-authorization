/*
 * Copyright (C) 2017-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.asclepios.authorization.abac.server.log;

import eu.asclepios.authorization.abac.server.log.entity.Attribute;
import eu.asclepios.authorization.abac.server.log.entity.Decision;
import eu.asclepios.authorization.abac.server.log.entity.Request;
import eu.asclepios.authorization.abac.server.log.repository.AttributeRepository;
import eu.asclepios.authorization.abac.server.log.repository.DecisionRepository;
import eu.asclepios.authorization.abac.server.log.repository.RequestRepository;
import eu.asclepios.authorization.abac.util.CheckAccessRequest;
import eu.asclepios.authorization.abac.util.PdpResult;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * LogDatabaseService
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class LogDatabaseService {
    private final RequestRepository requestRepository;
    private final DecisionRepository decisionRepository;
    private final AttributeRepository attributeRepository;
    private final Map<String,Long> requestRefToId = new HashMap<>();

    public void logRequest(String requestRef, CheckAccessRequest accessRequest) {
        // Log authorization request
        Request request = new Request(requestRef, accessRequest.getId());
        requestRepository.save(request);
        long requestId = request.getId();
        requestRefToId.put(requestRef, requestId);
        log.debug("New Request record: {}", request);

        // Log authorization request attributes
        for (Map.Entry<String,Object> entry : accessRequest.getXacmlRequest().entrySet()) {
            String name = entry.getKey();
            String value = null;
            String type = null;
            if (entry.getValue()!=null) {
                value = entry.getValue().toString();
                type = entry.getValue().getClass().getSimpleName();
            }
            logAttribute(requestId, requestRef, null, name, value, type, "REQUEST");
        }
    }

    public void logResult(String requestRef, PdpResult results) {
        // Log authorization results and final decision
        long requestPk = getRequestPk(requestRef);
        Decision finalDecision = new Decision(requestPk, results.getDecision(), results.getStatusCodes(), results.getStatusMessage());
        decisionRepository.save(finalDecision);
        requestRefToId.remove(requestRef);
        log.debug("New final Decision record: {}", finalDecision);

        // Mark request as closed
        Request request = requestRepository.findById(requestPk);
        if (request!=null) {
            request.setCompleted();
            requestRepository.save(request);
            log.debug("Request record marked as completed: {}", request);
        }

        // Log partial decisions
        for (Map.Entry<String, Map<String, String>> obligation : results.getObligations().entrySet()) {
            String obligationId = obligation.getKey();
            Map<String, String> obligationAttributes = obligation.getValue();

            String sourceType = obligationAttributes.get("asclepios:obligation:type");
            String sourceId = obligationAttributes.get("asclepios:obligation:id");
            String sourceName = obligationAttributes.get("asclepios:obligation:name");
            String decision = obligationAttributes.get("asclepios:obligation:decision");
            String sourceParent = obligationAttributes.get("asclepios:obligation:parent");

            Decision partialDecision = new Decision(requestPk, decision, sourceType, sourceId, sourceName, sourceParent);
            decisionRepository.save(partialDecision);
            log.debug("New partial Decision record: {}", partialDecision);
        }
    }

    public void logAttribute(String requestRef, Attribute attribute) {
        long requestPk = getRequestPk(requestRef);
        attribute.setRequestId(requestPk);
        attributeRepository.save(attribute);
        log.debug("New Attribute record: {}", attribute);
    }

    public void logAttribute(long requestId, String requestRef, String category, String id, String value, String type, String issuer) {
        // Log attribute
        Attribute attribute = new Attribute(requestId, category, id, value, type, issuer);
        logAttribute(requestRef, attribute);
    }

    private long getRequestPk(String requestId) {
        Long pk = requestRefToId.get(requestId);
        if (pk==null) {
            List<Request> results = requestRepository.findByUniqueRef(requestId);
            if (results==null || results.size()!=1)
                throw new RuntimeException("Zero or more than one Requests found, with UniqueRef: "+requestId);
            pk = results.get(0).getId();
        }
        return pk;
    }
}
