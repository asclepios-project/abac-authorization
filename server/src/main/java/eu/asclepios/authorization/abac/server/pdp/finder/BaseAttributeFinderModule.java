/*
 * Copyright (C) 2017-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.asclepios.authorization.abac.server.pdp.finder;

import eu.asclepios.authorization.abac.server.pdp.PDPRequestCtx;
import lombok.extern.slf4j.Slf4j;
import org.wso2.balana.attr.AttributeValue;
import org.wso2.balana.attr.BagAttribute;
import org.wso2.balana.attr.DateTimeAttribute;
import org.wso2.balana.attr.StringAttribute;
import org.wso2.balana.cond.EvaluationResult;
import org.wso2.balana.ctx.EvaluationCtx;
import org.wso2.balana.utils.Constants.PolicyConstants;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Base Attribute Finder. It must be extended.
 * Provides a few helper methods to its subclasses.
 */
@Slf4j
public abstract class BaseAttributeFinderModule extends org.wso2.balana.finder.AttributeFinderModule {

	private static final String EMPTY_STRING_VALUE = "";
	private static final URI STRING_URI = URI.create(PolicyConstants.DataType.STRING);
	private static final URI DATE_TIME_URI = URI.create(PolicyConstants.DataType.DATE_TIME);

	/**
	 * Helper method for checking whether the expected attribute id, category, and type match the given
	 * @param expectedType Expected attribute type
	 * @param givenType Given attribute type
	 * @param expectedId Expected attribute id
	 * @param givenId Given attribute id
	 * @param expectedCategory Expected attribute category
	 * @param givenCategory Given attribute category
	 * @return True if all attribute parameters (id, category, type) match
	 */
	protected boolean attributeMatches(String expectedType, URI givenType,
									   String expectedId, URI givenId,
									   String expectedCategory, URI givenCategory)
	{
		log.debug("attributeMatches: \n\ttype: {} <-> {}, \n\tid: {} <-> {}, \n\tcategory: {} <-> {}",
				expectedType, givenType, expectedId, givenId, expectedCategory, givenCategory);

		boolean match = true;
		if (!expectedType.equals(givenType.toASCIIString())) {
			log.debug("Attribute Type is not supported: {}", givenType);
			match = false;
		}
		if (!expectedId.equals(givenId.toASCIIString())) {
			log.debug("Attribute Id is not supported: {}", givenId);
			match = false;
		}
		if (!expectedCategory.equals(givenCategory.toASCIIString())) {
			log.debug("Attribute Category is not supported: {}", givenCategory);
			match = false;
		}

		log.debug("Attribute {}matched: type={}, id={}, category={}",
				match ? "" : "not ", givenType, givenId, givenCategory);
		return match;
	}

	/**
	 * Helper method for extracting the <b>Request id</b> (if any) from evaluation context
	 * <p>
	 * Request id is stored as an Environment string attribute with id <b><i>request-context-id</i></b>
	 * </p>
	 * @param context The evaluation context
	 * @return The <b>Request id</b> stored in evaluation context or null if not set
	 */
	/*protected String getRequestId(EvaluationCtx context) {
		if (context.getRequestCtx()!=null && context.getRequestCtx() instanceof PDPRequestCtx) {
			return ((PDPRequestCtx)context.getRequestCtx()).getRequestId();
		}

		for (Attributes attrs : context.getRequestCtx().getAttributesSet()) {
			if (attrs != null && XACMLConstants.ENT_CATEGORY.equals(attrs.getCategory().toASCIIString())) {
				String id = attrs.getAttributes().stream()
						.filter(att -> "request-context-id".equals(att.getId().toASCIIString()))
						.map(att -> att.getValue().encode())
						.findFirst().orElse(null);
				return id;
			}
		}
		return null;
	}*/

	/**
	 * Creates an empty Bag attribute, indicating that no results are returned
	 * This effectively means that AttributeFinder can not provide attribute value
	 * (e.g. not supported or unknown attribute) so attribute search must continue
	 * @param attributeType The bag attribute type
	 * @return Returns an empty bag attribute result of the specified type
	 */
	protected EvaluationResult createEmptyResult(URI attributeType) {
		return new EvaluationResult(BagAttribute.createEmptyBag(attributeType));
	}

	/**
	 * This method is similar to {@link #createStringResult(String)} but it also
	 * logs all attribute and request information passed in
	 * @See #createStringResult(String)
	 * @param category Attribute category
	 * @param type Attribute type
	 * @param attributeId Attribute id
	 * @param value Attribute value. The resulting string bag will contain a single string attribute with this value
	 * @param context Evaluation context
	 * @return Returns a bag attribute containing a single string attribute with the given value
	 */
	protected EvaluationResult createStringResult(URI category, URI type, URI attributeId, String value, EvaluationCtx context) {
		String requestId = null;
		if (context.getRequestCtx() instanceof PDPRequestCtx) {
			PDPRequestCtx reqCtx = (PDPRequestCtx)context.getRequestCtx();
			if (reqCtx!=null) {
				requestId = reqCtx.getRequestId();
				reqCtx.getLogService().logAttribute(category, type, attributeId, value, this.getClass().getSimpleName(), requestId);
			}
		}

		log.info("--------    ATTRIBUTE: finder={}, request-id={}, category={}, type={}, attr-id={}, value={}",
				this.getClass().getName(), requestId, category, type, attributeId, value);
		return createStringResult(value);
	}

	/**
	 * This method is similar to {@link #createStringResult(URI, URI, URI, String, EvaluationCtx)} but
	 * accepts multiple values to add in results bag and log each one of them
	 * @See #createStringResult(String)
	 * @param category Attribute category
	 * @param type Attribute type
	 * @param attributeId Attribute id
	 * @param context Evaluation context
	 * @param values Attribute values. The resulting string bag will contain all string values passed in
	 * @return Returns a bag attribute containing multiple string attributes with the given values
	 */
	protected EvaluationResult createStringResult(URI category, URI type, URI attributeId, EvaluationCtx context, String...values) {
		PDPRequestCtx reqCtx = null;
		String requestId = null;
		if (context.getRequestCtx() instanceof PDPRequestCtx) {
			reqCtx = (PDPRequestCtx)context.getRequestCtx();
			if (reqCtx!=null) {
				requestId = reqCtx.getRequestId();
			}
		}

		List<AttributeValue> bag = new ArrayList<>();
		if (values!=null) {
			for (String value : values) {
				log.debug("Adding attribute value to bag: {}", value);
				if (reqCtx!=null && requestId!=null)
					reqCtx.getLogService().logAttribute(category, type, attributeId, value, this.getClass().getSimpleName(), requestId);
				bag.add(new StringAttribute(value));
			}
		}
		log.info("--------    ATTRIBUTE: finder={}, request-id={}, category={}, type={}, attr-id={}, value={}",
				this.getClass().getName(), "requestId", category, type, attributeId,
				values!=null ? Arrays.asList(values) : null);
		return new EvaluationResult(new BagAttribute(STRING_URI, bag));
	}

	/**
	 * Creates a String bag attribute containing a single string attribute
	 * with the given value
	 * @param value The value of the string attribute in the bag
	 * @return Returns a bag attribute containing a single string attribute with the given value
	 */
	protected EvaluationResult createStringResult(String value) {
		log.debug("Returning attribute value: {}", value);
		List<AttributeValue> bag = new ArrayList<>();
		bag.add(new StringAttribute( value ));
		return new EvaluationResult(new BagAttribute(STRING_URI, bag));
	}

	/**
	 * Creates a String bag attribute containing a single string attribute with empty string value
	 * <p>
	 * Note-1: It should be used when an AttributeFinder supports the given attribute but for some reason it cannot
	 * provide a value. Returning an empty string will prevent an INDETERMINANT policy evaluation result due to
	 * MISSING_ATTRIBUTE error
	 * </p>
	 * <p>
	 * Note-2: If an empty string value is not valid or appropriate result then {@link #createStringResult(String)}
	 * with a suitable default value should be preferred
	 * </p>
	 * @return Returns a bag attribute containing a single string attribute with empty string value
	 */
	protected EvaluationResult createEmptyStringResult() {
		return createStringResult(EMPTY_STRING_VALUE);
	}

	/**
	 * This method is similar to {@link #createDateTimeResult(Date)} but it also
	 * logs all attribute and request information passed in
	 * @See #createStringResult(String)
	 * @param category Attribute category
	 * @param type Attribute type
	 * @param attributeId Attribute id
	 * @param value Attribute value. The resulting dateTime bag will contain a single dateTime attribute with this value
	 * @param context Evaluation context
	 * @return Returns a bag attribute containing a single dateTime attribute with the given value
	 */
	protected EvaluationResult createDateTimeResult(URI category, URI type, URI attributeId, Date value, EvaluationCtx context) {
		String requestId = null;
		if (context.getRequestCtx() instanceof PDPRequestCtx) {
			PDPRequestCtx reqCtx = (PDPRequestCtx) context.getRequestCtx();
			if (reqCtx != null) {
				requestId = reqCtx.getRequestId();
				reqCtx.getLogService().logAttribute(category, type, attributeId, value.toString(), this.getClass().getSimpleName(), requestId);
			}
		}

		log.info("--------    ATTRIBUTE: finder={}, request-id={}, category={}, type={}, attr-id={}, value={}",
				this.getClass().getName(), requestId, category, type, attributeId, value);
		return createDateTimeResult(value);
	}

	/**
	 * Creates a DateTime bag attribute containing a single dateTime attribute
	 * with the given value
	 * @param value The value of the dateTime attribute in the bag
	 * @return Returns a bag attribute containing a single dateTime attribute with the given value
	 */
	protected EvaluationResult createDateTimeResult(Date value) {
		log.debug("Returning attribute value (Date): {}", value);
		List<AttributeValue> bag = new ArrayList<>();
		bag.add(new DateTimeAttribute( value ));
		return new EvaluationResult(new BagAttribute(URI.create(PolicyConstants.DataType.DATE_TIME), bag));
	}

	/**
	 * Creates an empty DateTime bag attribute
	 * @return Returns an empty DateTime bag attribute
	 */
	protected EvaluationResult createEmptyDateTimeResult() { return createEmptyResult(DATE_TIME_URI); }

	/**
	 * Helper method for searching for a string attribute value from an AttributeFinder
	 * @param attributeType Type of the attribute searching for
	 * @param attributeId Id of the attribute searching for
	 * @param issuer Issuer of the attribute searching for (can be null)
	 * @param category Category of the attribute searching for
	 * @param context The evaluation context of the original AttributeFinder call
	 * @return Returns the attribute value or null if not found
	 */
	protected String findStringAttribute(URI attributeType, URI attributeId,
								 String issuer, URI category, EvaluationCtx context)
	{
		String value = null;
		//log.warn(">>>>>  type={}, id={}, issuer={}, category={}", attributeType, attributeId, issuer, category);
		AttributeValue result = context
				.getAttribute(attributeType, attributeId, issuer, category)
				.getAttributeValue();
		//log.warn(">>>>>  result={}", result);
		if (result.returnsBag()) {
			BagAttribute bag = (BagAttribute)result;
			//log.warn(">>>>>  IT'S A BAG: {}", bag);
			//log.warn(">>>>>  BAG LENGTH: {}", bag.size());
			if (!bag.isEmpty()) {
				Object o = bag.iterator().next();
				//log.warn(">>>>>  BAG NOT EMPTY: {}", o);
				if (o instanceof StringAttribute) {
					StringAttribute sa = (StringAttribute)o;
					//log.warn(">>>>>  BAG ENTRY IS STRING-ATTR: {}", sa);
					value = sa.getValue();
				} else {
					//log.warn(">>>>>  BAG ENTRY IS A {}: {}", o.getClass().getName(), o);
				}
			}
		} else
		if (result instanceof StringAttribute) {
			StringAttribute sa = (StringAttribute)result;
			//log.warn(">>>>>  IT'S A STRING-ATTR: {}", sa);
			value = sa.getValue();
		} else {
			//log.warn(">>>>>  IT'S A {}: {}", result.getClass().getName(), result);
		}
		//log.warn(">>>>>  RETURN VALUE: {}", value);
		return value;
	}
}
