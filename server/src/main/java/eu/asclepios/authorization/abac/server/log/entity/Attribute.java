/*
 * Copyright (C) 2017-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.asclepios.authorization.abac.server.log.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@NoArgsConstructor
public class Attribute implements Serializable {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    private Long timestamp;
    private Long requestId;
    private String attributeCategory;
    private String attributeId;
    @Column(length = 1000)
    private String attributeValue;
    private String attributeType;
    private String attributeIssuer;

    public Attribute(long requestId, String category, String id, String value, String type, String issuer) {
        this.timestamp = System.currentTimeMillis();
        this.requestId = requestId;
        this.attributeCategory = category;
        this.attributeId = id;
        this.attributeValue = StringUtils.substring(value, 0, 1000);
        this.attributeType = type;
        this.attributeIssuer = issuer;
    }
}
