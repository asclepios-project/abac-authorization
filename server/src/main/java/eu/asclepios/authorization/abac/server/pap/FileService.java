/*
 * Copyright (C) 2017-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.asclepios.authorization.abac.server.pap;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringSubstitutor;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Slf4j
@Service
public class FileService {

	public List<String> listPolicies(String policyDir, Pattern pattern) throws IOException {
		return Files.list(Paths.get(policyDir))
				.filter(Files::isRegularFile)
				.map(Path::toString)
				//.peek(System.out::println)
				.map(name -> {
					Matcher mat = pattern.matcher(name);
					String result = null;
					while (mat.find()) {
						int c = mat.groupCount();
						if (c > 0) result = mat.group(c);
					}
					return result;
				})
				//.peek(System.out::println)
				.filter(Objects::nonNull)
				//.peek(System.out::println)
				.collect(Collectors.toList());
	}

	public String getPolicyFileName(String policyId, String policyTypeName, String pattern) {
		// check if policy id is valid
		if (StringUtils.isBlank(policyId))
			throw new IllegalArgumentException(policyTypeName+" id is blank or null");
		if (! policyId.matches("^[0-9A-Za-z-_]+$"))
			throw new IllegalArgumentException(policyTypeName+" id is invalid: "+policyId);

		// generate policy file name
		Map<String,String> info = new HashMap<>();
		info.put("POLICY_ID", policyId);
		info.put("TIMESTAMP", Long.toString(System.currentTimeMillis()));
		String fileName = StringSubstitutor.replace(pattern.replace("%{","${"), info);

		return fileName;
	}

	public Path getPolicyFile(String policyId, String policyTypeName, String pattern, String policyDir, boolean exceptionIfNotExists) {
		String fileName = getPolicyFileName(policyId, policyTypeName, pattern);
		Path policiesDirectory = Paths.get(policyDir);
		if (!policiesDirectory.toFile().exists())
			throw new RuntimeException("Configuration Error: "+policyTypeName+" directory does not exist: "+policiesDirectory);
		Path policyFile = Paths.get(policiesDirectory.toString(), fileName);
		if (exceptionIfNotExists && ! Files.exists(policyFile)) {
			throw new RuntimeException("Policy does not exist: " + policyTypeName + " with id " + policyId);
		}
		return policyFile;
	}

	public String readPolicy(String policyId, String methodName, String policyTypeName, String pattern, String policyDir) throws IOException {
		log.info("{}: Reading {} with id: {}", methodName, policyTypeName, policyId);

		// read policy from the file system
		Path policyFile = getPolicyFile(policyId, policyTypeName, pattern, policyDir, true);
		String policyContents = new String(Files.readAllBytes(policyFile), StandardCharsets.UTF_8);
		log.warn("{}: {} read from file: id: {}, file: {}", methodName, policyTypeName, policyId, policyFile);

		return policyContents;
	}

	public void storePolicy(String policyId, String policyContent, String methodName, String policyTypeName, String pattern, String policyDir) throws IOException {
		log.info("{}: Adding {} with id: {}", methodName, policyTypeName, policyId);
		log.debug("{}: New {} content:\n{}", methodName, policyTypeName, policyContent);

		// save new policy to the file system
		Path policyFile = getPolicyFile(policyId, policyTypeName, pattern, policyDir, false);
		if (Files.exists(policyFile)) {
			log.warn("{}: Replacing {} with id: {}, file: {}", methodName, policyTypeName, policyId, policyFile);
		}
		Files.write(policyFile, policyContent.getBytes(StandardCharsets.UTF_8),
				StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.WRITE);
		log.warn("{}: {} stored to file: id: {}, file: {}", methodName, policyTypeName, policyId, policyFile);
	}

	public boolean deletePolicy(String policyId, String methodName, String policyTypeName, String pattern, String policyDir) throws IOException {
		log.info("{}: Reading {} with id: {}", methodName, policyTypeName, policyId);

		// delete policy from the file system
		Path policyFile = getPolicyFile(policyId, policyTypeName, pattern, policyDir, true);
		boolean deleted = Files.deleteIfExists(policyFile);
		deleted = ! Files.exists(policyFile);
		log.warn("{}: {} deleted: flag={}, id: {}, file: {}", methodName, deleted, policyTypeName, policyId, policyFile);

		return deleted;
	}
}
