/*
 * Copyright (C) 2017-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.asclepios.authorization.abac.server.properties;

import eu.asclepios.authorization.abac.util.properties.ContextClientProperties;
import eu.asclepios.authorization.abac.util.properties.DatabaseClientProperties;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Data
@Validated
@Configuration
@ConfigurationProperties
@PropertySources({
		@PropertySource(value = "file:${CONFIG_DIR}/authorization-server.properties")
})
public class AuthorizationServiceProperties {

	@NotNull
	private PdpConfig pdp;
	
	private ContextConfig context;
	
	private List<PipConfig> pip = new ArrayList<>();
	
	private ContextClientProperties requestContext;

	// --------------------------------------------------------------
	
	@Data
	public static class PdpConfig {
		private boolean enable = true;
		@NotBlank
		private String balanaConfigFile;
		@NotBlank
		private String policiesDir;
		@NotBlank
		private String policiesExtension = ".xml";
		@Min(1)
		private long restartPollingInterval = 1000;
		@NotBlank
		private String accessKey;

		private List<String> attributeFinderClasses;
	}
	
	@Data
	public static class ContextConfig {
		private boolean enable = true;
		private boolean softDelete = true;
		@NotNull
		private DatabaseClientProperties db;
		@NotBlank
		private String contextTable = "context_elements";
		@NotBlank
		private String requestsTable = "requests";
	}
	
	@Data
	public static class PipConfig {
		@NotNull
		private DatabaseClientProperties db;
	}
	
	// --------------------------------------------------------------
	
	public static boolean booleanValue(String str) {
		return booleanValue(str, false);
	}
	
	public static boolean booleanValue(String str, boolean defVal) {
		if (str==null || (str=str.trim()).isEmpty()) return defVal;
		return (str.equalsIgnoreCase("true") || str.equalsIgnoreCase("yes") || str.equalsIgnoreCase("on"));
	}
}
