/*
 * Copyright (C) 2017-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.asclepios.authorization.abac.server.log.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
@NoArgsConstructor
public class Decision {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    private Long timestamp;
    private Long requestId;
    private String decision;
    private String status;
    private String message;

    private boolean partial;
    private String sourceType;
    private String sourceId;
    private String sourceName;
    private String sourceParent;

    public Decision(long requestId, String decision, String status, String message) {
        this.timestamp = System.currentTimeMillis();
        this.requestId = requestId;
        this.decision = decision;
        this.status = status;
        this.message = message;
        this.partial = false;
    }

    public Decision(long requestId, String decision, String sourceType, String sourceId, String sourceName, String sourceParent) {
        this.timestamp = System.currentTimeMillis();
        this.requestId = requestId;
        this.decision = decision;
        this.partial = true;

        this.sourceType = sourceType;
        this.sourceId = sourceId;
        this.sourceName = sourceName;
        this.sourceParent = sourceParent;
    }
}
