#
# Copyright (C) 2017-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
#
# This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
# If a copy of the MPL was not distributed with this file, You can obtain one at
# https://www.mozilla.org/en-US/MPL/2.0/
#

FROM openjdk:8-jdk-alpine

# Added to enable file listing in IntelliJ IDEA Docker plugin
# Can be safely removed
RUN apk add coreutils

ENV ABAC_SERVER_BASE=/abac-server

RUN mkdir -p ${ABAC_SERVER_BASE} \
    ${ABAC_SERVER_BASE}/config \
    ${ABAC_SERVER_BASE}/policies \
    ${ABAC_SERVER_BASE}/logs
WORKDIR ${ABAC_SERVER_BASE}

ADD *.jar ${ABAC_SERVER_BASE}/
ADD *.sh ${ABAC_SERVER_BASE}/
ADD *.txt ${ABAC_SERVER_BASE}/
ADD config ${ABAC_SERVER_BASE}/samples/config/
ADD policies/examples/permit* ${ABAC_SERVER_BASE}/samples/policies/
ADD LICENSE* ${ABAC_SERVER_BASE}/
ADD README* ${ABAC_SERVER_BASE}/

RUN chmod +x *.sh

#ENTRYPOINT ["/bin/sh"]
ENTRYPOINT ["./run.sh"]