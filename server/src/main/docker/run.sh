#!/bin/sh
#
# Copyright (C) 2017-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
#
# This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
# If a copy of the MPL was not distributed with this file, You can obtain one at
# https://www.mozilla.org/en-US/MPL/2.0/
#

echo "*** ABAC Authorization Server ***"
echo

BASEDIR=$(dirname "$0")
POLICIES_DIR=$BASEDIR/policies
CONFIG_DIR=$BASEDIR/config
echo Config. Dir.: $CONFIG_DIR
echo Policies Dir: $POLICIES_DIR
echo

# Copy sample configuration if missing
if [ ! -f "$CONFIG_DIR/authorization-server.properties" ]; then
  echo No configuration file found. Copying default configuration and keystores.
  echo Any existing files will not be altered.
  cp -n -v samples/config/* $CONFIG_DIR/
  echo
fi

# Copy a sample policy if none is available
if [[ $(find $POLICIES_DIR -maxdepth 1 -name "*.xml" | wc -l) == "0" ]]; then
  echo There are no policies in Policies directory. Copying a sample, permit-all policy.
  echo Any existing files will not be altered.
  cp -n -v samples/policies/* $POLICIES_DIR
  echo
fi

# Run Docker policy dir. monitoring script
# Usage: policies-mon.sh <Delay in sec> <Policy dir. and file pattern>
./policies-mon.sh 5 "$POLICIES_DIR/*.xml" &

# Run ABAC server
export CONFIG_DIR
#java -jar abac-authorization-server-exec.jar --spring.config.location=file:$CONFIG_DIR/authorization-server.properties

if [[ ! -z "$ABAC_SERVER_LIB_PATH" ]]; then
    LOADER_PATH=-Dloader.path=$ABAC_SERVER_LIB_PATH
    echo External Lib. path: $ABAC_SERVER_LIB_PATH
fi
java -cp abac-authorization-server-exec.jar  $LOADER_PATH  org.springframework.boot.loader.PropertiesLauncher --spring.config.location=file:$CONFIG_DIR/authorization-server.properties
