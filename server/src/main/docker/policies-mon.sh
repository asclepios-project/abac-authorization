#!/bin/sh
#
# Copyright (C) 2017-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
#
# This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
# If a copy of the MPL was not distributed with this file, You can obtain one at
# https://www.mozilla.org/en-US/MPL/2.0/
#

echo "Monitoring: $2  Polling every $1s..."
LTIME=`ls -l --time-style=full-iso $2 2>/dev/null 1| sha1sum`
while true
do
   ATIME=`ls -l --time-style=full-iso $2 2>/dev/null 1| sha1sum`
   if [[ "$ATIME" != "$LTIME" ]]
   then
       echo "Policies changed..."
       kill -HUP `pidof java`
       LTIME=$ATIME
   fi
   sleep $1
done