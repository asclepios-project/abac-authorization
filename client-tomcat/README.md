<!--
  ~
  ~ Copyright (C) 2017-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
  ~
  ~ This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
  ~ If a copy of the MPL was not distributed with this file, You can obtain one at
  ~ https://www.mozilla.org/en-US/MPL/2.0/
  ~
  -->
# ASCLEPIOS ABAC Authorization Client Library (PEP) for standalone Tomcat

Please refer to [ABAC Authorization Client library][1] README file for information
on downloading, building and using ABAC client library.

[1]: ../client/README.md "ABAC Authroization Client library"

This module provides a JAR file containing ABAC client library, suitable for
use with a standalone Tomcat server.

## Add ABAC client (PEP) in a standalone Tomcat

1. Copy JAR file at `abac-authorization/client-tomcat/target/abac-authorization-client-for-tomcat-3.0.3-jar-with-dependencies.jar`
   into Tomcat libraries directory (e.g. `${CATALINA_HOME}/libs`).
   
    ```
    cp abac-authorization/client-tomcat/target/abac-authorization-client-for-tomcat-3.0.3-jar-with-dependencies.jar ${CATALINA_HOME}/libs 
    ```

2. Copy the truststore file of ABAC server into Tomcat home directory (`CATALINA_HOME`).
   You can copy it at any location, but you will need to change the following commands accordingly.
   
    ```
    cp abac-authorization/server/src/main/resources/config/truststore.p12 ${CATALINA_HOME}/ 
    ```

3. Add the following lines in `web.xml` file of Tomcat (at `${CATALINA}/conf/` directory) in order to wire ABAC client's Servlet filter into Tomcat's request processing flow.

    ```
    <filter>
        <filter-name>AsclepiosAbacFilter</filter-name>
        <filter-class>eu.asclepios.authorization.abac.client.AuthorizationServiceServletFilter</filter-class>
    </filter>
    <filter-mapping>
        <filter-name>AsclepiosAbacFilter</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>
    ```

    Modifying `url-pattern` it's possible to protected specific Tomcat contexts and applications.
    It's also possible to add previous lines in an application's `web.xml` file rather than Tomcat's in order to protect that particular application.

4. Set environment variables for configuring ABAC client. The following environment variables can be set
   in command line just before starting up Tomcat.

    In Linux:
    
    ```
    AZ_CALL_DISABLED=false
    AZ_CALL_LOAD_BALANCE_METHOD=ORDER
    AZ_CALL_RETRIES=1
    AZ_SERVER_ENDPOINTS=https://localhost:7071/checkJsonAccessRequest
    AZ_SERVER_ACCESS_KEY=7235687126587231675321756752657236156321765723
    AZ_CLIENT_TRUST_STORE_FILE=truststore-client.p12
    AZ_CLIENT_TRUST_STORE_TYPE=PKCS12
    AZ_CLIENT_TRUST_STORE_PASSWORD=asclepios
    
    export AZ_CLIENT_TRUST_STORE_FILE AZ_CLIENT_TRUST_STORE_TYPE AZ_CLIENT_TRUST_STORE_PASSWORD AZ_SERVER_ENDPOINTS AZ_SERVER_ACCESS_KEY AZ_CALL_DISABLED AZ_CALL_LOAD_BALANCE_METHOD AZ_CALL_RETRIES
    $CATALINE_HOME/bin/startup.sh
    ```

    In Windows:
    
    ```
    set AZ_CALL_DISABLED=false
    set AZ_CALL_LOAD_BALANCE_METHOD=ORDER
    set AZ_CALL_RETRIES=1
    set AZ_SERVER_ENDPOINTS=https://localhost:7071/checkJsonAccessRequest
    set AZ_SERVER_ACCESS_KEY=7235687126587231675321756752657236156321765723
    set AZ_CLIENT_TRUST_STORE_FILE=truststore-client.p12
    set AZ_CLIENT_TRUST_STORE_TYPE=PKCS12
    set AZ_CLIENT_TRUST_STORE_PASSWORD=asclepios

    %CATALINE_HOME%\bin\startup.bat
    ```

After completing these tasks, any application deployed in Tomcat will be protected by ABAC client.

---